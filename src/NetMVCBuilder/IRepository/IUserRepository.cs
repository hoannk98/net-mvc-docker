﻿using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.RequestModel;
using System.Collections.Generic;

namespace NetMVCBuilder.IRepository
{
    public interface IUserRepository
    {
        int Add(User user);
        int Delete(int id);
        User GetById(int? id);
        int Update(User user);
        UserRequestModel GetByUser(string KeySearch, int page, int pageSize);
        User GetByUserName(string UserName, int? id);
        int EditInfo(User user);
        int ChangePassword(int id, string PassWord);

        #region Phần quản lý tin nhắn liên hệ
        List<MessageContact> GetMessageContact(MessageContactRequestModel request, int pageIndex, int recordPerPage, out int totalCount);
        int SendMessage(MessageContact model);
        #endregion
    }
}
