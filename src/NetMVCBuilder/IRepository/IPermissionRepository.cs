﻿using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetMVCBuilder.IRepository
{
    public interface IPermissionRepository
    {
        PermissionActionInfo GetPermissionActionInfo();
        int SavePermission(PermissionAction model);
        List<Role> GetAllRole();
        List<User> GetAllUser();
        List<UserRole> GetAllUserRole();
        int SaveUserRole(UserRoleInfo model);
        int SaveRole(Role model);
    }
}
