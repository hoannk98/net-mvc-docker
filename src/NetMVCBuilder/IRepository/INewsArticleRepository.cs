﻿using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.InfoModel;
using NetMVCBuilder.Models.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetMVCBuilder.IRepository
{
    interface INewsArticleRepository
    {
        List<NewsArticle> Search(NewsArticleRequestModel request, int pageIndex, int recordPerPage, out int totalCount);
        List<NewsArticle> SearchNewsTrash(NewsArticleRequestModel request, int pageIndex, int recordPerPage, out int totalCount);
        int Delete(int id);
        int DeleteForever(int id);
        int Restore(int id);
        List<NewsArticleInfo> GetListAll(int? newsCategoryId);
        List<ProductImage> AddImage(List<ProductImage> img);

        NewsArticle GetArticleByUrlSegment(string UrlSegment, int id);
        NewsArticle GetById(int id);
        int Update(NewsArticleInfo info);
    }
}
