﻿using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.InfoModel;
using NetMVCBuilder.Models.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetMVCBuilder.IRepository
{
    public interface IProductRepository
    {
        int Add(ProductInfo sanPham);
        int AddProductDetail(ProductDetail ct);
        List<ProductInfo> GetByProduct(ProductRequestModel request, int pageIndex, int recordPerPage, out int totalCount);
        Product GetByMaSanPham(string ma_san_pham, int? id);
        Product GetByTenSanPham(string ten_san_pham, int? id);
        ProductDetail CheckDuplicatedChiTietSanPham(int? size, int? color, int san_pham_id);
        List<ProductDetailInfo> GetListDetailByProductId(ProductRequestModel request, int pageIndex, int recordPerPage, out int totalCount);
        int DeleteProductDetail(int id);
        ProductInfo GetById(int id);
        int Edit(ProductInfo sanPham);
        int Delete(int id);
    }
}
