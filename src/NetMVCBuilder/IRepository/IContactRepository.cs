﻿using NetMVCBuilder.Models.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetMVCBuilder.IRepository
{
    public interface IContactRepository
    {
        int AddorUpdate(Contact contact);
        Contact GetInfo();
    }
}
