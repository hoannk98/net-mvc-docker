﻿using NetMVCBuilder.Models.RequestModel;
using System;
using System.Collections.Generic;
using NetMVCBuilder.Models.EntityModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetMVCBuilder.IRepository
{
    public interface IColorRepository
    {
        int Add(Color size);
        List<Color> GetByColor(ColorRequestModel request, int pageIndex, int recordPerPage, out int totalCount);
        Color CheckDuplicatedColor(string size);
        Color GetById(int? id);
    }
}
