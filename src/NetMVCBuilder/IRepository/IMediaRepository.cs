﻿using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetMVCBuilder.IRepository
{
    public interface IMediaRepository
    {
        List<ProductImage> Search(MediaSelectorSearchQuery request, int pageIndex, int recordPerPage, out int totalCount);
        ProductImage GetById(string id);
        ProductImage GetByUrl(string url);
    }
}
