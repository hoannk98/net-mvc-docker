﻿using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetMVCBuilder.IRepository
{
    public interface ISizeRepository
    {
        int Add(Size size);
        List<Size> GetBySize(SizeRequestModel request, int pageIndex, int recordPerPage, out int totalCount);
        Size GetByTenSize(string size);
        Size GetById(int? id);
    }
}
