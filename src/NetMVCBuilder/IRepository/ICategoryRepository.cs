﻿using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetMVCBuilder.IRepository
{
    internal interface ICategoryRepository
    {
        int AddOrUpdate(Category size);
        List<Category> GetByCategory(CategoryRequestModel request, int pageIndex, int recordPerPage, out int totalCount);
        Category CheckDuplicatedCategory(string name);
        List<Category> GetAll();
        Category GetById(int? id);
    }
}
