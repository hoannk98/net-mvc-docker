/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    console.log(config)
    // Define changes to default configuration here. For example:
    config.language = 'vi';
    config.uiColor = '#AADC6E';
    config.height = 400;
    config.extraPlugins = 'youtube';
    //You can also change the default plugin options on config.js:

    //Video width:

    config.youtube_width = '640';

    //Video height:

    config.youtube_height = '480';

    // Make responsive (ignore width and height, fit to width):

    config.youtube_responsive = true;

    //Show related videos:

    config.youtube_related = false;

    //Use old embed code:

    config.youtube_older = false;

    // Enable privacy- enhanced mode:

    config.youtube_privacy = false;

    // Start video automatically:

    config.youtube_autoplay = false;

    // Show player controls:

    config.youtube_controls = true;

    CKEDITOR.dtd.$removeEmpty['i'] = 0;
    //config.protectedSource.push(/<i[^>]*>.*?<\/i>/g);
    //config.extraPlugins = 'videoembed';
    //https://docs.ckeditor.com/ckeditor4/docs/#!/api/CKEDITOR.config-cfg-embed_provider
    config.allowedContent = true;
    ////config.allowedContent = 'i|b|u|sup|sub|ol|ul|li|br|h1|h2|h3|h4|h5|p|div|span|img|strong|section';
    // ALLOW <i></i>

    config.disallowedContent = 'script; *[on*]';
    config.embed_provider = '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}';

    //config.contentsCss = [
    //    '/Themes/ViettelIDC/vendor/bootstrap/css/bootstrap.min.css',
    //    '/Themes/ViettelIDC/vendor/fontawesome/css/font-awesome.min.css',
    //    '/Themes/ViettelIDC/vendor/animateit/animate.min.css',
    //    '/Themes/ViettelIDC/css/theme-base.css',
    //    '/Themes/ViettelIDC/css/theme-elements.css',
    //    '/Themes/ViettelIDC/css/responsive.css',
    //    '/Themes/ViettelIDC/css/slick.css',
    //    '/Themes/ViettelIDC/css/slick-theme.css',
    //    '/Themes/ViettelIDC/css/color-variations/blue.css',
    //    '/Themes/ViettelIDC/css/custom.css'];

   
    config.contentsCss = ['/Content/Editors/ckeditor/ttf/Roboto/Roboto.css', '/Content/th.css'];
    console.log(config.contentsCss);
    console.log(config.font_names);
    config.toolbar = [
        { name: 'document', items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates'] },
        { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
        { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'] },
        { name: 'forms', items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'] },
        '/',
        { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat'] },
        { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language'] },
        { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },

        '/',
        { name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Youtube'] },//'Iframe',
        { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize', 'lineheight'] },
        { name: 'colors', items: ['TextColor', 'BGColor'] },
        { name: 'tools', items: ['Maximize', 'ShowBlocks'] }
    ]; 

    config.extraPlugins = 'lineheight';
    config.line_height = "1em;1.1em;1.2em;1.3em;1.4em;1.5em";
     

};   