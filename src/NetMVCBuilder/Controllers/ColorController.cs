﻿using NetMVCBuilder.Common;
using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.Enum;
using NetMVCBuilder.Models.RequestModel;
using NetMVCBuilder.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetMVCBuilder.Controllers
{
    public class ColorController : BaseController
    {
        private IColorRepository _colorRepository = new ColorRepository();
        // Index
        public ActionResult Index(ColorRequestModel model)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            model.page = model.page == 0 ? 1 : model.page;
            model.pageSize = 20;
            int totalCount = 0;

            ViewBag.hasViewPermission = CheckPermission((int)PageId.QuanLyCategory, (int)Actions.Xem, currentUser);
            ViewBag.hasAddPermission = CheckPermission((int)PageId.QuanLyCategory, (int)Actions.Them, currentUser);
            ViewBag.hasUpdatePermission = CheckPermission((int)PageId.QuanLyCategory, (int)Actions.Sua, currentUser);
            ViewBag.hasDeletePermission = CheckPermission((int)PageId.QuanLyCategory, (int)Actions.Xoa, currentUser);
            if (!ViewBag.hasViewPermission)
            {
                return RedirectToAction("NotPermission", "Home");
            }

            model.data = _colorRepository.GetByColor(model, model.page, model.pageSize, out totalCount);
            model.totalRecord = totalCount;
            model.totalPage = (int)Math.Ceiling((decimal)model.totalRecord / model.pageSize);
            return View(model);
        }
        // Add
        public ActionResult Add(int? id)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var checkPer = CheckPermission((int)PageId.QuanLyColor, (int)Actions.Them, currentUser);
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền
            Color rs = _colorRepository.GetById(id);
            Color color = new Color()
            {
                Id = rs.Id,
                Name = rs.Name?.Trim(),
            };
            return View(color);
        }
        [HttpPost]
        public ActionResult Add(Color color)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var checkPer = CheckPermission((int)PageId.QuanLyColor, (int)Actions.Them, currentUser);
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền

            var result = _colorRepository.Add(color);
            return Json(result == 1 ? new { status = 1, message = "Thêm thành công" } : new { status = 0, message = "Thêm thất bại" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckDuplicatedColor(string color)
        {
            var ma = _colorRepository.CheckDuplicatedColor(color);
            return Json(ma == null, JsonRequestBehavior.AllowGet);
        }
    }
}