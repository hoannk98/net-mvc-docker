﻿using Namek.Core.Utility;
using NetMVCBuilder.Common;
using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.Enum;
using NetMVCBuilder.Models.InfoModel;
using NetMVCBuilder.Models.RequestModel;
using NetMVCBuilder.Repository;
using NetMVCBuilder.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetMVCBuilder.Controllers
{
    public class NewsArticleController : BaseController
    {
        private static INewsArticleRepository _news = new NewsArticleRepository();

        public ActionResult Index(NewsArticleRequestModel model)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];

            model.page = model.page == 0 ? 1 : model.page;
            model.pageSize = 20;
            int totalCount = 0;
            ViewBag.hasViewPermission = CheckPermission((int)PageId.QuanLyTinBai, (int)Actions.Xem, currentUser);
            ViewBag.hasAddPermission = CheckPermission((int)PageId.QuanLyTinBai, (int)Actions.Them, currentUser);
            ViewBag.hasUpdatePermission = CheckPermission((int)PageId.QuanLyTinBai, (int)Actions.Sua, currentUser);
            ViewBag.hasDeletePermission = CheckPermission((int)PageId.QuanLyTinBai, (int)Actions.Xoa, currentUser);
            if (!ViewBag.hasViewPermission)
            {
                return RedirectToAction("NotPermission", "Home");
            }

            model.data = _news.Search(model, model.page, model.pageSize, out totalCount);
            model.totalRecord = totalCount;
            model.totalPage = (int)Math.Ceiling((decimal)model.totalRecord / model.pageSize);
            return View(model);
        }

        public ActionResult ViewTrash(NewsArticleRequestModel model)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            ViewBag.hasViewPermission = CheckPermission((int)PageId.QuanLyTinBai, (int)Actions.Xem, currentUser);
            ViewBag.hasAddPermission = CheckPermission((int)PageId.QuanLyTinBai, (int)Actions.Them, currentUser);
            ViewBag.hasUpdatePermission = CheckPermission((int)PageId.QuanLyTinBai, (int)Actions.Sua, currentUser);
            ViewBag.hasDeletePermission = CheckPermission((int)PageId.QuanLyTinBai, (int)Actions.Xoa, currentUser);
            if (!ViewBag.hasViewPermission)
            {
                return RedirectToAction("NotPermission", "Home");
            }
            model.page = model.page == 0 ? 1 : model.page;
            model.pageSize = 20;
            int totalCount = 0;
            model.data = _news.SearchNewsTrash(model, model.page, model.pageSize, out totalCount);
            model.totalRecord = totalCount;
            model.totalPage = (int)Math.Ceiling((decimal)model.totalRecord / model.pageSize);
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var checkPer = CheckPermission((int)PageId.QuanLyTinBai, (int)Actions.Them, currentUser);
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền
            NewsArticle model = _news.GetById(id);
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Update(NewsArticleInfo news)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var checkPer = CheckPermission((int)PageId.QuanLyTinBai, (int)Actions.Sua, currentUser);
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền
            //vailidate input
            if (news.Name == "" || news.Name.Length == 0) { return Json(new { status = 0, message = "Bạn chưa nhập tên bài viết" }, JsonRequestBehavior.AllowGet); }
            if (news.Name.Length >= 1000) { return Json(new { status = 0, message = "Tên bài viết quá dài" }, JsonRequestBehavior.AllowGet); }
            if (news.Summary == "" || news.Summary.Length == 0) { return Json(new { status = 0, message = "Bạn chưa nhập mô tả bài viết" }, JsonRequestBehavior.AllowGet); }
            if (news.Summary.Length >= 2000) { return Json(new { status = 0, message = "Mô tả bài viết quá dài" }, JsonRequestBehavior.AllowGet); }
            if (news.ContentNews == "" || news.ContentNews.Length == 0) { return Json(new { status = 0, message = "Bạn chưa nhập nội dung bài viết" }, JsonRequestBehavior.AllowGet); }
            //thêm mới nhưng k nhập file ảnh thumbnail
            if (news.Id <= 0 && news.PictureFiles != null && news.PictureFiles.ContentLength <= 0) { return Json(new { status = 0, message = "Bạn chưa thêm ảnh thumbnail cho bài viết" }, JsonRequestBehavior.AllowGet); }

            // convert anh thumbnail to base64
            if (news.PictureFiles != null && news.PictureFiles.ContentLength > 0)
            {
                using (System.Drawing.Image image = System.Drawing.Image.FromStream(news.PictureFiles.InputStream))
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        image.Save(m, image.RawFormat);
                        byte[] imageBytes = m.ToArray();
                        news.PictureThumbnail = Convert.ToBase64String(imageBytes); 
                    }
                }
            }
            news.UrlSegment = "/tin-tuc/" + SeoExtensions.GetSeName(news.Name);
            news.CreatedBy = currentUser.id;
            news.ModifiedBy = currentUser.id;

            var result = _news.Update(news);
            return Json(result == 1 ? new { status = 1, message = "Thêm thành công" } : new { status = 0, message = "Thêm thất bại" }, JsonRequestBehavior.AllowGet);
        }
        
        public string UploadImageThumbnail(HttpPostedFileBase fileUpload)
        {
            if (fileUpload == null) { return ""; }
            if (!Utils.CheckFileExtension(fileUpload.FileName, out var extension))
                throw new InvalidDataException($"File extension is denied.");
            var fileId = Guid.NewGuid().ToString();
            System.Drawing.Image image = System.Drawing.Image.FromStream(fileUpload.InputStream);
            System.Drawing.Image imageResize = Utils.resizeImageThumbnail(image);

            byte[] byteImg = Utils.convertImagetoByte(imageResize);

            var appData = Server.MapPath("~/Images/Thumbnail");
            if (!Directory.Exists(appData)) { Directory.CreateDirectory(appData); }
            var fileName = Path.GetFileName(fileId) + $".{extension}";
            var file = Path.Combine(appData, Path.GetFileName(fileId)) + $".{extension}";
            System.IO.File.WriteAllBytes(file, byteImg);
            return "/Images/Thumbnail/" + fileName;
        }

        [HttpPost]
        public int DeleteFile(string relativePath)
        {
            var rootFolder = ConfigurationManager.AppSettings["ImageFolder"];
            var path = rootFolder + "\\" + relativePath;
            FileInfo fileInfo = new FileInfo(path);
            fileInfo.Delete();
            return 1;
        }

        public ActionResult Delete(int id)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var checkPer = CheckPermission((int)PageId.QuanLyTinBai, (int)Actions.Xoa, currentUser);
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền xóa

            var result = _news.Delete(id);
            return Json(result == 1 ? new { status = 1, message = "Xóa thành công" } : new { status = 1, message = "Xóa thất bại" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteForever(int id)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var checkPer = CheckPermission((int)PageId.QuanLyTinBai, (int)Actions.Xoa, currentUser);
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền xóa

            var result = _news.DeleteForever(id);
            return Json(result == 1 ? new { status = 1, message = "Xóa thành công" } : new { status = 1, message = "Xóa thất bại" }, JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult Restore(int id)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var checkPer = CheckPermission((int)PageId.QuanLyTinBai, (int)Actions.Xoa, currentUser);
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền xóa

            var result = _news.Restore(id);
            return Json(result == 1 ? new { status = 1, message = "Xóa thành công" } : new { status = 0, message = "Xóa thất bại" }, JsonRequestBehavior.AllowGet);
        }

    }
}