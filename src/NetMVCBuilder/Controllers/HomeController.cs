﻿using NetMVCBuilder.Common;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.InfoModel;
using NetMVCBuilder.Models.RequestModel;
using NetMVCBuilder.Repository;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace NetMVCBuilder.Controllers
{
    public class HomeController : Controller
    {
        private CategoryRepository _cateRepo = new CategoryRepository();
        private NewsArticleRepository _newArtiRepo = new NewsArticleRepository();
        private UserRepository _useRepo = new UserRepository();
        private ProductRepository _proRepo = new ProductRepository();
        private ContactRepository _contactRepo = new ContactRepository();

        public ActionResult Index()
        {
            var newsArticle = new NewsArticleRequestModel();
            newsArticle.isHome = true;
            var model = new ProductRequestModel();

            //model.ListNewProduct = _proRepo.GetByProduct(model, 1, 6, out int totalCount);
            ViewBag.ListNewProduct = _proRepo.GetBySanPham(null, null, 1, 6).data;
            ViewBag.PhoneNumber = _contactRepo.GetInfo().PhoneNumber;
            model.ListNewsArticle = _newArtiRepo.Search(newsArticle, 1, 3, out int totalCount2);
            return View(model);
        }

        public ActionResult NotPermission()
        {
            return View();
        }

        [HttpGet]
        public ActionResult DetailsProduct(int id)
        {
            Product sanPham = new Product();
            return View("DetailsProduct", sanPham);
        }

        [HttpGet]
        public ActionResult GetInfoHomeLayout()
        {
            var rs = _cateRepo.GetAll();
            var lstSelect = rs.ConvertAll(x => new SelectListItem()
            {
                Value = x.Id + "",
                Text = x.Name + "",
            }).ToList() ?? new List<SelectListItem>();

            var contactInfo = _contactRepo.GetInfo();
            var result = new
            {
                lstSelect = lstSelect,
                contactInfo = contactInfo,
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult NewsArticleIndex(int? id)
        {
            List<NewsArticleInfo> lstNewsArticle = new List<NewsArticleInfo>();
            lstNewsArticle = _newArtiRepo.GetListAll(id);
            return View("NewsArticleIndex", lstNewsArticle);
        }

        public ActionResult SendMessage(MessageContact model)
        {
            var result = _useRepo.SendMessage(model);
            if (result > 0)
            {
                //gửi email
                SendEmail(model);
            }
            return Json(result == 1 ? new { status = 1, message = "Gửi thành công" } : new { status = 0, message = "Gửi thất bại" }, JsonRequestBehavior.AllowGet);
        }

        public void SendEmail(MessageContact model)
        {
            string from = ConfigurationManager.AppSettings["FromAddress"].ToString();
            string pass = ConfigurationManager.AppSettings["PasswordFromAddress"].ToString();
            string host = ConfigurationManager.AppSettings["HostMail"].ToString();
            string name = ConfigurationManager.AppSettings["NameDisplayEmail"].ToString();
            string to = ConfigurationManager.AppSettings["ToEmail"].ToString();

            string strSubject = $"CÓ KHÁCH HÀNG MỚI ĐĂNG KÝ NHẬN THÔNG TIN TỪ BẠN - GIAYXP.COM";
            string strMsg = "<b>XIN CHÀO BẠN!!!</b><br />" +
                            "Có một khách hàng mới đã đăng ký nhận thông tin từ bạn vào lúc " + DateTime.Now.ToString("dd/MM/yyyyy HH:mm:ss") + " <br /><br />" +
                            "<b>Thông tin chi tiết khách hàng:</b><br />" +
                            "Họ và tên: " + Server.HtmlEncode(model.Name?.Trim()) + " <br />" +
                            "Địa chỉ emai: " + Server.HtmlEncode(model.Email?.Trim()) + " <br />" +
                            "Tin nhắn: " + Server.HtmlEncode(model.Message?.Trim()) + " <br />" +
                            "Trân trọng!<br /><br /><a target='_blank' href='#'>{0}</a>";

            MailAddress fromAddress = new MailAddress(from, name);
            MailAddress toAddress = new MailAddress(to, from);

            SmtpClient smtp = new SmtpClient
            {
                Host = host,
                Port = 587,
                EnableSsl = true,
                UseDefaultCredentials = true,
                Credentials = new NetworkCredential(fromAddress.Address, pass),
                DeliveryMethod = SmtpDeliveryMethod.Network,
            };

            using (MailMessage mailMessage = new MailMessage(fromAddress, toAddress)
            {
                Subject = strSubject,
                Body = strMsg,
                IsBodyHtml = true,
            })
            {
                try
                {
                    mailMessage.AlternateViews.Add(GetEmbeddedImage(strMsg));
                    smtp.Send(mailMessage);
                }
                catch (Exception ex)
                {
                }
            }
        }

        public AlternateView GetEmbeddedImage(string email)
        {
            var baseDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
            string path = Path.Combine(baseDirectory, "assets", "img", "xp logo.png");
            LinkedResource res = new LinkedResource(path);
            res.ContentId = Guid.NewGuid().ToString();
            string img = @"<img src='cid:" + res.ContentId + @"'/>";
            email = String.Format(email, img);
            AlternateView alternateView = AlternateView.CreateAlternateViewFromString(email, null, MediaTypeNames.Text.Html);
            alternateView.LinkedResources.Add(res);
            return alternateView;
        }

        [Route("{ id : int, urlSegment: string}")]
        public ActionResult NewsArticleDetail(int id, string urlSegment)
        {
            NewsArticle article = new NewsArticle();
            article = _newArtiRepo.GetArticleByUrlSegment(urlSegment, id);
            return View("NewsArticleDetail", article);
        }
    }
}