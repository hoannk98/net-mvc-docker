﻿using NetMVCBuilder.Common;
using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.Enum;
using NetMVCBuilder.Models.RequestModel;
using NetMVCBuilder.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetMVCBuilder.Controllers
{
    public class CategoryController : BaseController
    {
        private ICategoryRepository _categoryRepository = new CategoryRepository();
        // Index
        public ActionResult Index(CategoryRequestModel model)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            model.page = model.page == 0 ? 1 : model.page;
            model.pageSize = 20;
            int totalCount = 0;

            ViewBag.hasViewPermission = CheckPermission((int)PageId.QuanLyCategory, (int)Actions.Xem, currentUser);
            ViewBag.hasAddPermission = CheckPermission((int)PageId.QuanLyCategory, (int)Actions.Them, currentUser);
            ViewBag.hasUpdatePermission = CheckPermission((int)PageId.QuanLyCategory, (int)Actions.Sua, currentUser);
            ViewBag.hasDeletePermission = CheckPermission((int)PageId.QuanLyCategory, (int)Actions.Xoa, currentUser);
            if (!ViewBag.hasViewPermission)
            {
                return RedirectToAction("NotPermission", "Home");
            }

            model.data = _categoryRepository.GetByCategory(model, model.page, model.pageSize, out totalCount);
            model.totalRecord = totalCount;
            model.totalPage = (int)Math.Ceiling((decimal)model.totalRecord / model.pageSize);
            return View(model);
        }
        // Add
        public ActionResult Add(int? id)
        {
            var checkPer = false;
            if (id == null || id <= 0){
                var currentUser = (User)Session[CommonConstants.USER_SESSION];
                checkPer = CheckPermission((int)PageId.QuanLyCategory, (int)Actions.Them, currentUser);
            }
            else{
                var currentUser = (User)Session[CommonConstants.USER_SESSION];
                checkPer = CheckPermission((int)PageId.QuanLyCategory, (int)Actions.Sua, currentUser);
                
            }
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền
            Category rs = _categoryRepository.GetById(id);
            Category category = new Category()
            {
                Id = rs.Id,
                Name = rs.Name?.Trim(),
            };
            return View(category);
        }
        [HttpPost]
        public ActionResult AddOrUpdate(Category category)
        {
            var checkPer = false;
            if (category.Id <= 0)
            {
                var currentUser = (User)Session[CommonConstants.USER_SESSION];
                checkPer = CheckPermission((int)PageId.QuanLyCategory, (int)Actions.Them, currentUser);
            }
            else
            {
                var currentUser = (User)Session[CommonConstants.USER_SESSION];
                checkPer = CheckPermission((int)PageId.QuanLyCategory, (int)Actions.Sua, currentUser);
            }
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền

            var result = _categoryRepository.AddOrUpdate(category);
            return Json(result == 1 ? new { status = 1, message = "Thêm thành công" } : new { status = 0, message = "Thêm thất bại" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckDuplicatedCategory(string name)
        {
            var ma = _categoryRepository.CheckDuplicatedCategory(name);
            return Json(ma == null, JsonRequestBehavior.AllowGet);
        }
    }
}