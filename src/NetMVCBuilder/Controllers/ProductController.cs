﻿using NetMVCBuilder.Common;
using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.Enum;
using NetMVCBuilder.Models.InfoModel;
using NetMVCBuilder.Models.RequestModel;
using NetMVCBuilder.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace NetMVCBuilder.Controllers
{
    public class ProductController : BaseController
    {
        private static IProductRepository _productRepository = new ProductRepository();
        private static ISizeRepository _sizeRepository = new SizeRepository();
        private static IColorRepository _colorRepository = new ColorRepository();
        private static ICategoryRepository _categoryRepository = new CategoryRepository();
        // GET: Product
        public ActionResult Index(ProductRequestModel model)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            model.page = model.page == 0 ? 1 : model.page;
            model.pageSize = 20;
            int totalCount = 0;
            ViewBag.hasViewPermission = CheckPermission((int)PageId.QuanLySanPham, (int)Actions.Xem, currentUser);
            ViewBag.hasAddPermission = CheckPermission((int)PageId.QuanLySanPham, (int)Actions.Them, currentUser);
            ViewBag.hasUpdatePermission = CheckPermission((int)PageId.QuanLySanPham, (int)Actions.Sua, currentUser);
            ViewBag.hasDeletePermission = CheckPermission((int)PageId.QuanLySanPham, (int)Actions.Xoa, currentUser);
            if (!ViewBag.hasViewPermission)
            {
                return RedirectToAction("NotPermission", "Home");
            }

            model.DataInfo = _productRepository.GetByProduct(model, model.page, model.pageSize, out totalCount);
            model.totalRecord = totalCount;
            model.totalPage = (int)Math.Ceiling((decimal)model.totalRecord / model.pageSize);
            return View(model);
        }

        public ActionResult Add()
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var checkPer = CheckPermission((int)PageId.QuanLySanPham, (int)Actions.Them, currentUser);
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(ProductInfo sanPham)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var checkPer = CheckPermission((int)PageId.QuanLySanPham, (int)Actions.Them, currentUser);
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền

            var lstChiTiet = sanPham.lstProductDetail ?? new List<ProductDetailInfo>();
            var group = lstChiTiet.GroupBy(x => new { x.SizeId, x.ColorId }).Select(g => new ProductDetailInfo()
            {
                ColorId = g.Key.ColorId,
                SizeId = g.Key.SizeId,
            }).ToList() ?? new List<ProductDetailInfo>();
            foreach (var item in group)
            {
                var dupplicate = lstChiTiet.Where(x => x.SizeId == item.SizeId && x.ColorId == item.ColorId).ToList() ?? new List<ProductDetailInfo>();
                if (dupplicate.Count > 1)
                {
                    return Json(new { status = -1, message = "Dữ liệu chi tiết sản phẩm đã bị trùng" }, JsonRequestBehavior.AllowGet);
                }
            }
            // product varition detail
            var deptList = JsonConvert.DeserializeObject<IList<ProductDetailInfo>>(sanPham.lstStrProductDetails);
            sanPham.lstProductDetail = deptList.ToList();

            // Xu ly Thumnail
            if (sanPham.PictureThumnailFile != null || sanPham.PictureThumnailFile.ContentLength > 0)
            {
                string _FileName = Path.GetFileName(sanPham.PictureThumnailFile.FileName);
                string filePath = "/Images/product/" + _FileName;
                string path = Path.Combine(Server.MapPath(filePath));
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
                sanPham.PictureThumnailFile.SaveAs(path);
                sanPham.PictureThumbnail = filePath;
            }
            // Xu ly Anh
            List<string> filePaths = new List<string>();
            foreach (HttpPostedFileBase file in sanPham.PictureFiles)
            {
                string _FileName = Path.GetFileName(file.FileName);
                string filePath = "/Images/product/" + _FileName;
                string path = Path.Combine(Server.MapPath(filePath));
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
                file.SaveAs(path);
                filePaths.Add(filePath);
            }
            sanPham.PicturePaths = filePaths;

            sanPham.ModifiedBy = currentUser.id;
            var result = _productRepository.Add(sanPham);
            return Json(result == 1 ? new { status = 1, message = "Thêm thành công" } : new { status = 0, message = "Thêm thất bại" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SearchSize(string q)
        {
            var model = new SizeRequestModel()
            {
                Keywords = q,
                page = 1,
                pageSize = 50
            };
            var result = _sizeRepository.GetBySize(model, model.page, model.pageSize, out var totalCount);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SearchColor(string q)
        {
            var model = new ColorRequestModel()
            {
                Keywords = q,
                page = 1,
                pageSize = 50
            };
            var result = _colorRepository.GetByColor(model, model.page, model.pageSize, out var totalCount);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckDuplicatedMaSanPham(string ma_san_pham, int? id)
        {
            var ma = _productRepository.GetByMaSanPham(ma_san_pham, id);
            return Json(ma == null, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckDuplicatedTenSanPham(string ten_san_pham, int? id)
        {
            var name = _productRepository.GetByTenSanPham(ten_san_pham, id);
            return Json(name == null, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckDuplicatedChiTietSanPham(int? size, int? color, int san_pham_id)
        {
            var name = _productRepository.CheckDuplicatedChiTietSanPham(size, color, san_pham_id);
            return Json(name == null, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult SearchProduct(string q)
        {
            var model = new ProductRequestModel()
            {
                Keywords = q,
                page = 1,
                pageSize = 50
            };
            var result = _productRepository.GetByProduct(model, model.page, model.pageSize, out var totalCount);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SearchChiTietSanPham(string q, int? productId)
        {
            var model = new ProductRequestModel()
            {
                Keywords = q,
                page = 1,
                pageSize = 20,
                ProductId = productId,
            };
            var result = _productRepository.GetListDetailByProductId(model, model.page, model.pageSize, out var totalCount);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult SearchCategory(string q)
        {
            var model = new CategoryRequestModel()
            {
                Keywords = q,
                page = 1,
                pageSize = 50
            };
            var result = _categoryRepository.GetByCategory(model, model.page, model.pageSize, out var totalCount);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddProductDetail(ProductDetail ct)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var checkPer = CheckPermission((int)PageId.QuanLySanPham, (int)Actions.Them, currentUser);
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền

            if (ct.ColorId <= 0 || ct.SizeId <= 0)
            {
                return Json(new { status = 0, message = "Thêm thất bại" }, JsonRequestBehavior.AllowGet);
            }
            ct.CreatedDate = DateTime.Now;
            ct.ModifiedDate = DateTime.Now;
            var result = _productRepository.AddProductDetail(ct);
            return Json(result == 1 ? new { status = 1, message = "Thêm thành công" } : new { status = 0, message = "Thêm thất bại" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteProductDetail(int id)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var checkPer = CheckPermission((int)PageId.QuanLySanPham, (int)Actions.Xoa, currentUser);
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền xóa

            var result = _productRepository.DeleteProductDetail(id);
            return Json(result == 1 ? new { status = 1, message = "Xóa thành công" } : new { status = 1, message = "Xóa thất bại" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var checkPer = CheckPermission((int)PageId.QuanLySanPham, (int)Actions.Sua, currentUser);
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền sửa

            ProductInfo productInfo = _productRepository.GetById(id);
            return View("Edit", productInfo);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(ProductInfo productInfo)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var lstChiTiet = productInfo.lstProductDetail ?? new List<ProductDetailInfo>();
            var group = lstChiTiet.GroupBy(x => new { x.SizeId, x.ColorId }).Select(g => new ProductDetailInfo()
            {
                ColorId = g.Key.ColorId,
                SizeId = g.Key.SizeId,
            }).ToList() ?? new List<ProductDetailInfo>();
            foreach (var item in group)
            {
                var dupplicate = lstChiTiet.Where(x => x.SizeId == item.SizeId && x.ColorId == item.ColorId).ToList() ?? new List<ProductDetailInfo>();
                if (dupplicate.Count > 1)
                {
                    return Json(new { status = -1, message = "Dữ liệu chi tiết sản phẩm đã bị trùng" }, JsonRequestBehavior.AllowGet);
                }
            }
            // product varition detail
            var deptList = JsonConvert.DeserializeObject<IList<ProductDetailInfo>>(productInfo.lstStrProductDetails);
            productInfo.lstProductDetail = deptList.ToList();

            // Xu ly Thumnail
            if (productInfo.PictureThumnailFile != null)
            {
                string _FileName = Path.GetFileName(productInfo.PictureThumnailFile.FileName);
                string filePath = "/Images/product/" + _FileName;
                string path = Path.Combine(Server.MapPath(filePath));
                if (System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
                productInfo.PictureThumnailFile.SaveAs(path);
                productInfo.PictureThumbnail = filePath;
            }
            // Xu ly Anh
            List<string> filePaths = new List<string>();
            if (productInfo.PictureFiles != null)
            {
                foreach (HttpPostedFileBase file in productInfo.PictureFiles)
                {
                    string _FileName = Path.GetFileName(file.FileName);
                    string filePath = "/Images/product/" + _FileName;
                    string path = Path.Combine(Server.MapPath(filePath));
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    file.SaveAs(path);
                    filePaths.Add(filePath);
                }
                productInfo.PicturePaths = filePaths;
            }

            productInfo.ModifiedBy = currentUser.id;
            var result = _productRepository.Edit(productInfo);
            return Json(result > 0 ? new { status = 1, message = "Cập nhật thành công" } : new { status = 0, message = "Cập nhật thất bại" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var checkPer = CheckPermission((int)PageId.QuanLySanPham, (int)Actions.Xoa, currentUser);
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền
            var result = _productRepository.Delete(id);
            return Json(result > 0 ? new { status = 1, message = "Cập nhật thành công" } : new { status = 0, message = "Cập nhật thất bại" }, JsonRequestBehavior.AllowGet);
        }
    }
}