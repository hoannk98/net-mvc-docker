﻿using NetMVCBuilder.Common;
using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.InfoModel;
using NetMVCBuilder.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetMVCBuilder.Controllers
{
    public class ContactController : Controller
    {
        private IContactRepository _contactRepository = new ContactRepository();
        // GET: Contact
        [HttpGet]
        public ActionResult Edit()
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            Contact contact = _contactRepository.GetInfo();
            ContactInfo contactInfo = new ContactInfo()
            {
                Id = contact.Id,
                CompanyName = contact.CompanyName,
                BusinessRegistrationCertificate = contact.BusinessRegistrationCertificate,
                CertificateDate = contact.CertificateDate,
                Address = contact.Address,
                Email = contact.Email,
                TiktokAddress = contact.TiktokAddress,
                 YouTubeAddress = contact.YouTubeAddress,
                FacebookAddress = contact.FacebookAddress,
                MessengerAddress = contact.MessengerAddress,
                 ZaloAddress = contact.ZaloAddress,
                CompanySummary = contact.CompanySummary,
                PhoneNumber = contact.PhoneNumber,
        };
            return View("Edit", contactInfo);
        }

        [HttpPost]
        public ActionResult Edit(Contact contact)
        {
            User user = (User)Session[CommonConstants.USER_SESSION];
            contact.CreatedBy = user.id;
            contact.ModifiedBy = user.id;
            

            var result = _contactRepository.AddorUpdate(contact);
            return Json(result == 1 ? new { status = 1, message = "Cập nhật thành công" } : new { status = 0, message = "Cập nhật thất bại" }, JsonRequestBehavior.AllowGet);
        }
    }
}