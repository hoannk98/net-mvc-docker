﻿using NetMVCBuilder.Common;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.InfoModel;
using NetMVCBuilder.Models.RequestModel;
using NetMVCBuilder.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using NetMVCBuilder.IRepository;

namespace NetMVCBuilder.Controllers
{
    public class AdminController : BaseController
    {
        private IPermissionRepository _permissionRepository = new PermissionRepository();

        // GET: Report
        public ActionResult PermissionManagerment()
        {
            var model = _permissionRepository.GetPermissionActionInfo();
            if (model == null) model = new PermissionActionInfo();
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            ViewBag.hasViewPermission = CheckPermission(12, 1, currentUser);
            ViewBag.hasAddPermission = CheckPermission(12, 2, currentUser);
            ViewBag.hasUpdatePermission = CheckPermission(12, 4, currentUser);
            ViewBag.hasDeletePermission = CheckPermission(12, 8, currentUser);
            if (!ViewBag.hasViewPermission)
            {
                return RedirectToAction("NotPermission", "Home");
            }
            return View(model);
        }

        public ActionResult RoleManagerment()
        {
            var model = new PermissionActionInfo();
            model.ListRole = _permissionRepository.GetAllRole();
            model.ListUser = _permissionRepository.GetAllUser();
            model.ListUserRole = _permissionRepository.GetAllUserRole();
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            ViewBag.hasViewPermission = CheckPermission(13, 1, currentUser);
            ViewBag.hasAddPermission = CheckPermission(13, 2, currentUser);
            ViewBag.hasUpdatePermission = CheckPermission(13, 4, currentUser);
            ViewBag.hasDeletePermission = CheckPermission(13, 8, currentUser);
            if (!ViewBag.hasViewPermission)
            {
                return RedirectToAction("NotPermission", "Home");
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult SavePermission(PermissionAction model)
        {
            var result = _permissionRepository.SavePermission(model);
            var msg = "";
            if (result > 0) { msg = "Cập nhật thành công"; }
            else { msg = "cập nhât thất bại"; }
            return Json(new { status = result, message = msg }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveUserRole(UserRoleInfo model)
        {
            var result = _permissionRepository.SaveUserRole(model);
            var msg = "";
            if (result > 0) { msg = "Cập nhật thành công"; }
            else { msg = "cập nhât thất bại"; }
            return Json(new { status = result, message = msg }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditRole(int id)
        {
            return View();
        }

        public ActionResult AddRole(Role model)
        {
            User user = (User)Session[CommonConstants.USER_SESSION];
            var result = _permissionRepository.SaveRole(model);
            if (result == 1)
            {
                return Json(new { status = 1, message = "Thêm thành công" }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { status = 0, message = "Thêm thất bại" }, JsonRequestBehavior.AllowGet);
        }

    }
}