﻿using NetMVCBuilder.Common;
using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.Enum;
using NetMVCBuilder.Models.InfoModel;
using NetMVCBuilder.Models.RequestModel;
using NetMVCBuilder.Repository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace NetMVCBuilder.Controllers
{
    public class MessageContactController : BaseController
    {
        private UserRepository _userRepo = new UserRepository();

        // GET: MessageContact
        public ActionResult Index(MessageContactRequestModel model)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            model.page = model.page == 0 ? 1 : model.page;
            model.pageSize = 20;
            int totalCount = 0;
            ViewBag.hasViewPermission = CheckPermission((int)PageId.QuanLyTinNhanLienHe, (int)Actions.Xem, currentUser);
            ViewBag.hasAddPermission = CheckPermission((int)PageId.QuanLyTinNhanLienHe, (int)Actions.Them, currentUser);
            ViewBag.hasUpdatePermission = CheckPermission((int)PageId.QuanLyTinNhanLienHe, (int)Actions.Sua, currentUser);
            ViewBag.hasDeletePermission = CheckPermission((int)PageId.QuanLyTinNhanLienHe, (int)Actions.Xoa, currentUser);
            if (!ViewBag.hasViewPermission)
            {
                return RedirectToAction("NotPermission", "Home");
            }

            model.Data = _userRepo.GetMessageContact(model, model.page, model.pageSize, out totalCount);
            model.totalRecord = totalCount;
            model.totalPage = (int)Math.Ceiling((decimal)model.totalRecord / model.pageSize);
            return View(model);
        }

        [HttpGet]
        public ActionResult GetMessageContactById(int id)
        {
            var result = _userRepo.GetMessageContactById(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            var checkPer = CheckPermission((int)PageId.QuanLyTinNhanLienHe, (int)Actions.Xoa, currentUser);
            if (checkPer == false) { return RedirectToAction("NotPermission", "Home"); }  ////check quyền
            var result = _userRepo.DeleteMessageContact(id);
            return Json(result == 1 ? new { status = 1, message = "Xóa thành công" } : new { status = 1, message = "Xóa thất bại" }, JsonRequestBehavior.AllowGet);
        }
    }
}