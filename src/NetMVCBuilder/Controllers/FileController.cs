﻿using Namek.Core.Utility;
using NetMVCBuilder.Common;
using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.RequestModel;
using NetMVCBuilder.Repository;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace NetMVCBuilder.Controllers
{
    public class FileController : Controller
    {
        private static INewsArticleRepository _news = new NewsArticleRepository();

        [HttpPost]
        [ActionName("Files")]
        public JsonResult Files(MediaSelectorSearchQuery mediaCategory)
        {
            var currentUser = (User)Session[CommonConstants.USER_SESSION];
            List<ProductImage> lst = new List<ProductImage>();
            foreach (string files in Request.Files)
            {
                var file1 = Request.Files[files];
                var stream = file1.InputStream;
                var FileName = file1.FileName.Replace(" ", "-"); // file name hiển thị cho người dùng

                string fileLocation = Server.MapPath("~/Images/ImageNews");
                if (!Directory.Exists(fileLocation)) { Directory.CreateDirectory(fileLocation); }

                var fileId = Guid.NewGuid().ToString(); //file name định danh lưu ở db và thư mục
                if (!Utils.CheckFileExtension(file1.FileName, out var extension))
                    throw new InvalidDataException($"File extension is denied.");

                System.Drawing.Image image = System.Drawing.Image.FromStream(stream);
                byte[] byteImg = Utils.convertImagetoByte(image);

                var fileName = Path.GetFileName(fileId) + $".{extension}";
                var file = Path.Combine(fileLocation, Path.GetFileName(fileId)) + $".{extension}";
                System.IO.File.WriteAllBytes(file, byteImg);
                
                ProductImage img = new ProductImage()
                {
                    ImagePath = "/Images/ImageNews/" + fileName,
                    NameDisplay = FileName,
                    ContentLength = stream.Length.ToString(),
                    CreatedBy = currentUser.id,
                    CreatedDate = DateTime.Now,
                    ModifiedDate = DateTime.Now,
                    ModifiedBy = currentUser.id,
                };
                lst.Add(img);
                stream.Dispose();
            }
            if (lst.Count > 0)
            {
                var rs = _news.AddImage(lst);
                return Json(rs, "text/html", Encoding.UTF8);
            }
            return Json(null, "text/html", Encoding.UTF8);
        }

    }
}