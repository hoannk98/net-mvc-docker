﻿using NetMVCBuilder.Common;
using NetMVCBuilder.Enum;
using NetMVCBuilder.Models;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Repository;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace NetMVCBuilder.Controllers
{
    internal class CaptchaResponse
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("error-codes")]
        public List<string> ErrorCodes { get; set; }
    }

    public class AccountController : Controller
    {
        #region Definitions

        //STATIC
        private const string SessionUserLoginFails = "UserLoginFails";
        private const string SessionUserCaptcha = "UserCaptcha";

        #endregion

        public LoginRepository _loginRepo = new LoginRepository();

        // GET: Account
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            //// check if logged in
            //if (Request.IsAuthenticated)
            //    return RedirectToAction("Index", "Home");

            // check for existing session
            if (Session[SessionUserLoginFails] != null || Session[SessionUserCaptcha] != null)
            {
                var loginInfo = new LoginModel
                {
                    FailedLogins = Convert.ToInt32(Session[SessionUserLoginFails])
                };
                return View("Login", loginInfo);
            }

            ViewBag.Redirect = returnUrl;

            return View("Login", new LoginModel());
        }

        [HttpPost]
        public ActionResult Login(LoginModel loginInfo, string returnUrl)
        {
            Session["loginInfo"] = loginInfo;

            if (ModelState.IsValid)
            {
                // validate session captcha
                if (Convert.ToInt32(Session[SessionUserLoginFails]) > Constant.MaxUserLoginFailesWithoutCapcha)
                {
                    if (loginInfo.Captcha == null)
                    {
                        ModelState.AddModelError("", "Captcha không hợp lệ");

                        loginInfo.Captcha = null;
                        if (ModelState.ContainsKey("Captcha"))
                            ModelState["Captcha"].Value = null;
                        loginInfo.FailedLogins = Convert.ToInt32(Session[SessionUserLoginFails]);
                        return View("Login", loginInfo);
                    }

                    if (Session[SessionUserCaptcha] == null || loginInfo.Captcha == null
                        || !Encryption.CheckCaptcha(loginInfo.Captcha.ToLower(), (string)Session[SessionUserCaptcha]))
                    {
                        ModelState.AddModelError("", "Captcha không hợp lệ");

                        loginInfo.Captcha = null;
                        if (ModelState.ContainsKey("Captcha"))
                            ModelState["Captcha"].Value = null;
                        loginInfo.FailedLogins = Convert.ToInt32(Session[SessionUserLoginFails]);
                        return View("Login", loginInfo);
                    }
                }

                var user = _loginRepo.GetUserByUserName(loginInfo.UserName);
                if (user != null)
                {
                    bool authenSuccess;
                    authenSuccess = Encryption.CheckPassword(loginInfo.Password, user.PassWord, "");
                    if (authenSuccess)
                    {
                        var pagemodul = _loginRepo.GetAccountModulPageInfo(user.id);
                        Session.Add(CommonConstants.USER_SESSION, user);
                        Session.Add(CommonConstants.PAGE_MODUL_SESSION, pagemodul);

                        if (IsLocalUrl(returnUrl))
                            return Redirect(returnUrl);

                        return RedirectToAction("Index", "Product");
                    }
                }
                return LoginFail(loginInfo);
            }
            loginInfo.Captcha = null;
            if (ModelState.ContainsKey("Captcha"))
                ModelState["Captcha"].Value = null;

            return View("Login", loginInfo);
        }

        /// <summary>
        ///     Checking return url is same with the current host or not
        /// </summary>
        /// <param name="url">return url string</param>
        /// <returns></returns>
        public static bool IsLocalUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
                return false;
            return url[0] == '/' && (url.Length == 1 ||
                                     url[1] != '/' && url[1] != '\\') || // "/" or "/foo" but not "//" or "/\"
                   url.Length > 1 &&
                   url[0] == '~' && url[1] == '/'; // "~/" or "~/foo"
        }

        [HttpGet]
        public ActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult SignUp(User model)
        {
            if (ModelState.IsValid)
            {
                bool result = _loginRepo.SignUp(model);
                if (result)
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            return View();
        }


        public ActionResult Logout()
        {
            Session.Remove(CommonConstants.USER_SESSION);
            Session.Remove(SessionUserLoginFails);
            Session.Remove(SessionUserCaptcha);
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public ActionResult CheckDuplicatedUserName(string UserName, int? id)
        {
            var name = _loginRepo.GetByUserName(UserName, id);
            if (name == null)
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CaptchaImage(string prefix, bool noisy = true)
        {
            var rand = new Random((int)DateTime.Now.Ticks);
            const string combination = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

            var captcha = new StringBuilder();

            for (var i = 0; i < 6; i++)
                captcha.Append(combination[rand.Next(combination.Length)]);

            // Set encrypted captcha to session
            string captchaSessionName = string.IsNullOrWhiteSpace(prefix) ? "captcha" : prefix;
            Session[captchaSessionName] = Encryption.EncryptCaptcha(captcha.ToString().ToLower());

            //image stream
            string z;
            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(120, 50))
            using (var gfx = Graphics.FromImage(bmp))
            {
                gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));

                //add noise
                if (noisy)
                {
                    int i;
                    var pen = new Pen(System.Drawing.Color.Yellow);
                    for (i = 1; i < 10; i++)
                    {
                        pen.Color = System.Drawing.Color.FromArgb(
                            rand.Next(0, 255),
                            rand.Next(0, 255),
                            rand.Next(0, 255));

                        var r = rand.Next(0, 120 / 3);
                        var x = rand.Next(0, 120);
                        var y = rand.Next(0, 50);

                        gfx.DrawEllipse(pen, x - r, y - r, r, r);
                    }
                }

                //add question
                gfx.DrawString(captcha.ToString(), new Font("Tahoma", 20), Brushes.Gray, 3, 6);

                //render as Jpeg
                bmp.Save(mem, ImageFormat.Jpeg);
                z = @"data:image/png;base64," + Convert.ToBase64String(mem.GetBuffer());
            }

            return Json(z, JsonRequestBehavior.AllowGet);
        }

        private ActionResult LoginFail(LoginModel loginInfo)
        {
            // redirect on failure
            ModelState.AddModelError("", "Tài khoản hoặc mật khẩu không chính xác!");
            ModelState.Remove("FailedLogins");

            // increase user login fails
            var userLoginFails = Session[SessionUserLoginFails] == null
                ? 1
                : Convert.ToInt32(Session[SessionUserLoginFails]) + 1;
            loginInfo.FailedLogins = userLoginFails;
            Session[SessionUserLoginFails] = userLoginFails;

            Session[SessionUserCaptcha] = null;

            loginInfo.Captcha = null;
            if (ModelState.ContainsKey("Captcha"))
                ModelState["Captcha"].Value = null;
            return View("Login", loginInfo);
        }
    }
}