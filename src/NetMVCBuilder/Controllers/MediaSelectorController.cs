﻿using Namek.Core.Utility;
using NetMVCBuilder.AttributeCustom;
using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.RequestModel;
using NetMVCBuilder.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetMVCBuilder.Controllers
{
    public class MediaSelectorController : BaseController
    {
        private static IMediaRepository _med = new MediaRepository();
        private static INewsArticleRepository _news = new NewsArticleRepository();
        public MediaSelectorController()
        {

        }
        [XFrameOptionsFilterAttribute]
        public ActionResult Show(MediaSelectorSearchQuery Query)
        {
            if (Query == null)
            {
                Query = new MediaSelectorSearchQuery();
                string request = Request["Query"];
                Query.Query = request;
            }
            Query.page = Query.page == 0 ? 1 : Query.page;
            Query.pageSize = 100;
            int totalCount = 0;
            Query.data = _med.Search(Query, Query.page, Query.pageSize, out totalCount);
            Query.totalRecord = totalCount;
            Query.totalPage = (int)Math.Ceiling((decimal)Query.totalRecord / Query.pageSize);

            return PartialView("Show", Query);
        }

        public JsonResult GetFileInfo(string value)
        {
            var mediafile = _med.GetById(value);

            var SelectedItemInfo = new
            {
                Url = mediafile.ImagePath?.Trim(),
            };
            return Json(SelectedItemInfo, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetImageData(string url)
        {
            string decodedUrl = Utils.DecodeUrlString(url);

            ProductImage image = _med.GetByUrl(decodedUrl);

            return Json(new { alt = image.NameDisplay?.Trim(), title = image.NameDisplay?.Trim(), url = image.ImagePath?.Trim() }, JsonRequestBehavior.AllowGet);
        }
    }
}