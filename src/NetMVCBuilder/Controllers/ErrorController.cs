﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetMVCBuilder.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index(string permission, string pages)
        {
            ViewBag.Permission = permission;
            ViewBag.Pages = pages;

            return View();
        }

        public ActionResult NotFound()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}