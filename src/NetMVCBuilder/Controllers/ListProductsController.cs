﻿using System.Web.Mvc;
using NetMVCBuilder.Repository;
using NetMVCBuilder.Models.RequestModel;

namespace NetMVCBuilder.Controllers
{
    public class ListProductsController : Controller
    {
        private ProductRepository _sanPhamRepository = new ProductRepository();
        private CategoryRepository categoryRepository = new CategoryRepository();
        private ContactRepository _contactRepo = new ContactRepository();

        // GET: ListProducts
        public ActionResult Index(string KeySearch, int? categoryId, int page = 1, int pageSize = 12)
        {
            ProductRequestModel model = _sanPhamRepository.GetBySanPham(KeySearch, categoryId, page, pageSize);
            
            ViewBag.listCategory = categoryRepository.GetAll().ToArray();
            ViewBag.categoryId = categoryId;

            return View(model);
        }

        [Route("{id: int}")]
        public ActionResult ProductDetail(int id)
        {
            Models.InfoModel.ProductInfo model = _sanPhamRepository.GetById(id);
            ViewBag.PhoneNumber = _contactRepo.GetInfo().PhoneNumber;
            return View(model);
        }

        [HttpGet]
        public ActionResult GetListProducts(string KeySearch, int? categoryId, int page = 1, int pageSize = 12)
        {
            ProductRequestModel model = _sanPhamRepository.GetBySanPham(KeySearch, categoryId, page, pageSize);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}