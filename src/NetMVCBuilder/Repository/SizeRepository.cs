﻿using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Repository
{
    public class SizeRepository : ISizeRepository
    {
        private MaterialsContext db = new MaterialsContext();
        public int Add(Size size)
        {
            try
            {
                var data = db.Sizes.FirstOrDefault(x => x.Id == size.Id);
                if (data == null) data = new Size();
                data.Name = size.Name;
                if (size.Id <= 0)
                {
                    db.Sizes.Add(data);
                }
                db.SaveChanges();
                return 1;
            }
            catch { return 0; }
        }

        public Size GetById(int? id)
        {
            return db.Sizes.FirstOrDefault(x => x.Id == id) ?? new Size();
        }

        public List<Size> GetBySize(SizeRequestModel request, int pageIndex, int recordPerPage, out int totalCount)
        {
            pageIndex = pageIndex - 1;

            var query = from s in db.Sizes
                        where (string.IsNullOrEmpty(request.Keywords) || s.Name.Contains(request.Keywords))
                        select s;

            totalCount = query.Count();
            return query.OrderByDescending(x => x.Id).Skip(pageIndex * recordPerPage).Take(recordPerPage).ToList() ?? new List<Size>();
        }

        public Size GetByTenSize(string size)
        {
            var data = db.Sizes.Where(x => x.Name.ToLower() == size.ToLower()).FirstOrDefault();
            return data;
        }
    }
}