﻿using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.InfoModel;
using NetMVCBuilder.Models.RequestModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;

namespace NetMVCBuilder.Repository
{
    public class UserRepository : IUserRepository
    {
        MaterialsContext db = new MaterialsContext();
        public int Add(User user)
        {
            try
            {
                user.CreatedDate = DateTime.Now;
                user.ModifyDate = DateTime.Now;
                db.Users.Add(user);
                db.SaveChanges();
                return 1;
            }
            catch { return 0; }
        }

        public int Delete(int id)
        {
            var data = db.Users.Where(x => x.id == id).FirstOrDefault();
            if (data == null) { return 0; }
            data.ModifyDate = DateTime.Now;
            data.IsDeleted = 1;
            db.SaveChanges();
            return 1;
        }

        public User GetById(int? id)
        {
            var data = db.Users.Where(x => x.IsDeleted == 0 && x.id == id).FirstOrDefault();
            if (data == null) { return new User(); }
            return data;
        }

        public UserRequestModel GetByUser(string KeySearch, int page, int pageSize)
        {
            UserRequestModel request = new UserRequestModel();
            List<User> lst = new List<User>();
            request.page = page;
            request.pageSize = pageSize;
            lst = db.Users.Where(x => x.IsDeleted == 0 && x.Position != "admin").ToList();
            int startRow = (page - 1) * pageSize;
            if (!string.IsNullOrEmpty(KeySearch))
            {
                KeySearch = KeySearch.ToLowerInvariant();
                lst = lst.Where(x => x.FullName.ToLowerInvariant().Contains(KeySearch) || 
                                     x.UserName.ToLowerInvariant().Contains(KeySearch)).ToList();
            }
            request.totalRecord = lst.Count;
            request.data = lst.OrderBy(x => x.id)
                              .Skip(startRow)
                              .Take(pageSize)
                              .ToList();
            int totalPage = 0;
            if (request.totalRecord % pageSize == 0)
            {
                totalPage = request.totalRecord / pageSize;
            }
            else
            {
                totalPage = request.totalRecord / pageSize + 1;
            }
            request.totalPage = totalPage;
            return request;
        }

        public int Update(User user)
        {
            var data = db.Users.Where(x => x.id == user.id).FirstOrDefault();
            if (data == null || data.UserName != user.UserName) { return 0; }
            data.id = user.id;
            data.FullName = user.FullName;
            data.UserName = user.UserName;
            data.IsDeleted = 0;
            data.UserGroupId = user.UserGroupId;
            data.PassWord = user.PassWord;
            data.ModifyDate = user.ModifyDate;
            data.ModifyBy = user.ModifyBy;
            db.SaveChanges();
            return 1;
        }

        public int EditInfo(User user)
        {
            var data = db.Users.Where(x => x.id == user.id).FirstOrDefault();
            if (data == null || data.UserName != user.UserName) { return 0; }
            data.id = user.id;
            data.FullName = user.FullName;
            data.IsDeleted = 0;
            data.ModifyDate = user.ModifyDate;
            data.ModifyBy = user.ModifyBy;
            db.SaveChanges();
            return 1;
        }

        public User GetByUserName(string UserName, int? id)
        {
            var data = db.Users.Where(x => x.IsDeleted == 0 && x.id != id && x.UserName == UserName).FirstOrDefault();
            return data;
        }
        public int ChangePassword(int id, string PassWord)
        {
            var data = db.Users.Where(x => x.id == id).FirstOrDefault();
            data.PassWord = PassWord;
            data.ModifyDate = DateTime.Now;
            db.SaveChanges();
            return 1;
        }



        #region Phần quản lý tin nhắn liên hệ
        public int SendMessage(MessageContact model)
        {
            model.CreateDate = DateTime.Now;
            model.ModifyDate = DateTime.Now;
            db.MessageContacts.Add(model);
            return db.SaveChanges();
        }

        public List<MessageContact> GetMessageContact(MessageContactRequestModel request, int pageIndex, int recordPerPage, out int totalCount)
        {
            pageIndex = pageIndex - 1;
            if (!string.IsNullOrEmpty(request.Keywords))
            {
                request.Keywords = request.Keywords.Trim();
            }

            var query = from s in db.MessageContacts
                        where (string.IsNullOrEmpty(request.Keywords) || s.Name.Contains(request.Keywords) || s.Email.Contains(request.Keywords))
                        select s;

            totalCount = query.Count();
            var rs = query.OrderByDescending(x => x.Id).Skip(pageIndex * recordPerPage).Take(recordPerPage).ToList();

            return rs ?? new List<MessageContact>();
        }

        public MessageContactInfo GetMessageContactById(int id)
        {
            var query = (from mc in db.MessageContacts
                         where mc.Id == id
                         select new MessageContactInfo()
                         {
                             Name = mc.Name,
                             Email = mc.Email,
                             CreateDate = mc.CreateDate,
                             ModifyDate = mc.ModifyDate,
                             Message = mc.Message,
                         }).FirstOrDefault() ?? new MessageContactInfo();
            query.CreateDateString = query.CreateDate.ToString("dd/MM/yyyy");
            query.ModifyDateString = query.ModifyDate.ToString("dd/MM/yyyy");

            return query;
        }

        public int DeleteMessageContact(int id)
        {
            var data = db.MessageContacts.Where(x => x.Id == id).FirstOrDefault();
            if (data == null) { return 0; }
            db.MessageContacts.Remove(data);
            return db.SaveChanges();
        }
        #endregion
    }
}