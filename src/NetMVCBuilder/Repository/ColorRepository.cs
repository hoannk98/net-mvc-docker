﻿using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Repository
{
    public class ColorRepository : IColorRepository
    {
        private MaterialsContext db = new MaterialsContext();
        public int Add(Color color)
        {
            try
            {
                var data = db.Colors.FirstOrDefault(x => x.Id == color.Id);
                if (data == null) data = new Color();

                data.Name = color.Name;
                if(color.Id <= 0)
                {
                    db.Colors.Add(data);
                }
                db.SaveChanges();
                return 1;
            }
            catch { return 0; }
        }

        public List<Color> GetByColor(ColorRequestModel request, int pageIndex, int recordPerPage, out int totalCount)
        {
            pageIndex = pageIndex - 1;

            var query = from s in db.Colors
                        where (string.IsNullOrEmpty(request.Keywords) || s.Name.Contains(request.Keywords))
                        select s;

            totalCount = query.Count();
            return query.OrderByDescending(x => x.Id).Skip(pageIndex * recordPerPage).Take(recordPerPage).ToList() ?? new List<Color>();
        }

        public Color CheckDuplicatedColor(string color)
        {
            var data = db.Colors.Where(x => x.Name.ToLower() == color.ToLower()).FirstOrDefault();
            return data;
        }

        public Color GetById(int? id)
        {
            return db.Colors.FirstOrDefault(x => x.Id == id) ?? new Color();
        }
    }
}