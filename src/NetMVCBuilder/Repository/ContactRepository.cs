﻿using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Repository
{
    public class ContactRepository : IContactRepository
    {
        MaterialsContext db = new MaterialsContext();
        public int AddorUpdate(Contact contact)
        {
            var data = db.Contacts.Where(x => x.Id == contact.Id).FirstOrDefault();
            if (data == null || contact.Id <= 0){
                contact.ModifiedDate = DateTime.Now;
                contact.CreatedDate = DateTime.Now;
                db.Contacts.Add(contact);
            }
            else {
                data.CompanyName = contact.CompanyName;
                data.BusinessRegistrationCertificate = contact.BusinessRegistrationCertificate;
                data.CertificateDate = contact.CertificateDate;
                data.Address = contact.Address;
                data.Email = contact.Email;
                data.TiktokAddress = contact.TiktokAddress;
                 data.YouTubeAddress = contact.YouTubeAddress;
                data.FacebookAddress = contact.FacebookAddress;
                 data.ZaloAddress = contact.ZaloAddress;
                data.MessengerAddress = contact.MessengerAddress;
                data.PhoneNumber = contact.PhoneNumber;
                data.CompanySummary = contact.CompanySummary;
                data.ModifiedDate = DateTime.Now;
                data.ModifiedBy = contact.ModifiedBy;
            }
            db.SaveChanges();
            return 1;
        }

        public Contact GetInfo()
        {
            var data = db.Contacts.FirstOrDefault();
            if (data == null) { return new Contact(); }
            return data;
        }
    }
}