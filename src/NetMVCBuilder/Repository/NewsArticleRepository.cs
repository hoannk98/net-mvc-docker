﻿using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.Enum;
using NetMVCBuilder.Models.InfoModel;
using NetMVCBuilder.Models.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Repository
{
    public class NewsArticleRepository : INewsArticleRepository
    {
        private MaterialsContext db = new MaterialsContext();

        public int Delete(int id)
        {
            var data = db.NewsArticles.FirstOrDefault(x => x.Id == id && x.IsDeleted != true);
            if (data == null) { return 0; }
            data.IsDeleted = true;
            return db.SaveChanges();
        }

        public int DeleteForever(int id)
        {
            var data = db.NewsArticles.FirstOrDefault(x => x.Id == id && x.IsDeleted == true);
            if (data == null) { return 0; }
            db.NewsArticles.Remove(data);
            return db.SaveChanges();
        }

        public List<NewsArticle> Search(NewsArticleRequestModel request, int pageIndex, int recordPerPage, out int totalCount)
        {
            List<NewsArticle> query = new List<NewsArticle>();
            pageIndex = pageIndex - 1;
            if (!string.IsNullOrEmpty(request.Keywords))
            {
                request.Keywords = request.Keywords.Trim();
            }
            if (request.isHome != null && request.isHome == true)
            {
                query = (from n in db.NewsArticles
                         where n.IsHot == 1 && n.Status == (int)NewsStatus.Publish && n.IsDeleted != true && (string.IsNullOrEmpty(request.Keywords) || n.Name.Contains(request.Keywords))
                         && ((!request.FromDate.HasValue && !request.ToDate.HasValue)
                        || (n.CreatedDate.Value >= request.FromDate.Value && !request.ToDate.HasValue)
                        || (n.CreatedDate.Value >= request.FromDate.Value && n.CreatedDate.Value <= request.ToDate.Value)
                        || (!request.FromDate.HasValue && n.CreatedDate.Value <= request.ToDate.Value))
                         select n).ToList();
            }
            else
            {
                query = (from n in db.NewsArticles
                         where n.IsDeleted != true && (string.IsNullOrEmpty(request.Keywords) || n.Name.Contains(request.Keywords))
                         && ((!request.FromDate.HasValue && !request.ToDate.HasValue)
                        || (n.CreatedDate.Value >= request.FromDate.Value && !request.ToDate.HasValue)
                        || (n.CreatedDate.Value >= request.FromDate.Value && n.CreatedDate.Value <= request.ToDate.Value)
                        || (!request.FromDate.HasValue && n.CreatedDate.Value <= request.ToDate.Value))
                         select n).ToList();
            }

            totalCount = query.Count();
            var rs = query.OrderByDescending(x => x.Id).Skip(pageIndex * recordPerPage).Take(recordPerPage).ToList();
            return rs ?? new List<NewsArticle>();
        }

        public List<NewsArticle> SearchNewsTrash(NewsArticleRequestModel request, int pageIndex, int recordPerPage, out int totalCount)
        {
            pageIndex = pageIndex - 1;
            if (!string.IsNullOrEmpty(request.Keywords))
            {
                request.Keywords = request.Keywords.Trim();
            }

            var query = (from n in db.NewsArticles
                         where n.IsDeleted == true && (string.IsNullOrEmpty(request.Keywords) || n.Name.Contains(request.Keywords))
                         && ((!request.FromDate.HasValue && !request.ToDate.HasValue)
                        || (n.DeletedDate.Value >= request.FromDate.Value && !request.ToDate.HasValue)
                        || (n.DeletedDate.Value >= request.FromDate.Value && n.DeletedDate.Value <= request.ToDate.Value)
                        || (!request.FromDate.HasValue && n.DeletedDate.Value <= request.ToDate.Value))
                         select n).ToList();

            totalCount = query.Count();
            var rs = query.OrderByDescending(x => x.Id).Skip(pageIndex * recordPerPage).Take(recordPerPage).ToList();
            return rs ?? new List<NewsArticle>();
        }

        public List<NewsArticleInfo> GetListAll(int? newsCategoryId)
        {
            var query = (from n in db.NewsArticles
                         join u in db.Users on n.CreatedBy equals u.id
                         where n.IsDeleted != true && n.Status == (int)NewsStatus.Publish
                         && (!newsCategoryId.HasValue || n.NewsCategoryId == newsCategoryId)
                         select new NewsArticleInfo()
                         {
                             Id = n.Id,
                             ContentNews = n.ContentNews,
                             Author = u.UserName,
                             CreatedBy = n.CreatedBy,
                             CreatedDate = n.CreatedDate,
                             IsHot = n.IsHot,
                             Name = n.Name,
                             Summary = n.Summary,
                             UrlSegment = n.UrlSegment,
                             Status = n.Status,
                             PictureThumbnail = n.PictureThumbnail,
                             NewsCategoryId = n.NewsCategoryId,
                             ModifiedDate = n.ModifiedDate,
                             Type = n.Type,
                             ModifiedBy = n.ModifiedBy,
                             DeletedDate = n.DeletedDate,
                             IsDeleted = n.IsDeleted
                         }).OrderByDescending(x => x.CreatedDate).ToList() ?? new List<NewsArticleInfo>();
            return query;
        }

        public NewsArticle GetArticleByUrlSegment(string UrlSegment, int id)
        {
            var query = from n in db.NewsArticles
                        where n.Id == id && n.IsDeleted != true && n.Status == (int)NewsStatus.Publish
                        select n;

            return query.FirstOrDefault() ?? new NewsArticle();
        }
        public List<ProductImage> AddImage(List<ProductImage> img)
        {
            List<ProductImage> lst = new List<ProductImage>();
            foreach (var item in img)
            {
                db.ProductImages.Add(item);
                db.SaveChanges();
                ProductImage imgProduct = new ProductImage()
                {
                    ContentLength = item.ContentLength,
                    Id = item.Id,
                    ImagePath = item.ImagePath,
                    ProductId = item.ProductId,
                    NameDisplay = item.NameDisplay,
                    CreatedDate = item.CreatedDate,
                };
                lst.Add(imgProduct);
            }
            return lst;
        }

        public NewsArticle GetById(int id)
        {
            return db.NewsArticles.FirstOrDefault(x => x.Id == id) ?? new NewsArticle();
        }

        public int Update(NewsArticleInfo info)
        {
            NewsArticle article = db.NewsArticles.FirstOrDefault(x => x.Id == info.Id);
            if (article == null) article = new NewsArticle();

            article.Name = info.Name;
            article.Summary = info.Summary;
            article.ContentNews = info.ContentNews;
            article.UrlSegment = info.UrlSegment;
            article.ModifiedBy = info.ModifiedBy;
            article.ModifiedDate = DateTime.Now;
            article.IsHot = info.IsHot;
            // nếu bài viết đó chưa xuất bản thì mới thay đổi trạng thái này đc
            if (article.Status != (int)NewsStatus.Publish)
            {
                article.Status = info.Status;
            }

            //nếu có thay ảnh thumbnail mới thì mới add
            if (info.PictureThumbnail != null && info.PictureThumbnail.Length > 0)
            {
                article.PictureThumbnail = info.PictureThumbnail;
            }

            if (info.Id <= 0)
            {
                article.CreatedBy = info.CreatedBy;
                article.CreatedDate = DateTime.Now;
                db.NewsArticles.Add(article);
            }

            return db.SaveChanges();
        }

        public int Restore(int id)
        {
            var data = db.NewsArticles.FirstOrDefault(x => x.Id == id);
            if (data == null) { return 0; }
            data.IsDeleted = false;
            return db.SaveChanges();
        }
    }
}