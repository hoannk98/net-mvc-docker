﻿using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Repository
{
    public class MediaRepository : IMediaRepository
    {
        private MaterialsContext db = new MaterialsContext();

        public ProductImage GetById(string id)
        {
            return db.ProductImages.FirstOrDefault(x => x.Id.ToString() == id) ?? new ProductImage();
        }

        public ProductImage GetByUrl(string url)
        {
            return db.ProductImages.FirstOrDefault(x => x.ImagePath == url) ?? new ProductImage();
        }

        public List<ProductImage> Search(MediaSelectorSearchQuery request, int pageIndex, int recordPerPage, out int totalCount)
        {
            pageIndex = pageIndex - 1;
            if (!string.IsNullOrEmpty(request.Query))
            {
                request.Query = request.Query.Trim();
            }
            var query = (from n in db.ProductImages
                         where (string.IsNullOrEmpty(request.Query) || n.NameDisplay.Contains(request.Query))
                         select n).ToList();

            totalCount = query.Count();
            var rs = query.OrderByDescending(x => x.Id).Skip(pageIndex * recordPerPage).Take(recordPerPage).ToList();
            return rs ?? new List<ProductImage>();
        }
    }
}