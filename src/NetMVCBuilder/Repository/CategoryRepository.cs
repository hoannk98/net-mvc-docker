﻿using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.RequestModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Repository
{
    public class CategoryRepository : ICategoryRepository
    {
        private MaterialsContext db = new MaterialsContext();
        public int AddOrUpdate(Category category)
        {
            try
            {
                var data = db.Categories.FirstOrDefault(x => x.Id == category.Id);
                if (data == null) data = new Category();

                data.Name = category.Name;
                category.ModifiedDate = DateTime.Now;
                if (category.Id == null || category.Id <= 0)
                {
                    category.CreatedDate = DateTime.Now;
                    
                    db.Categories.Add(data);
                }
                db.SaveChanges();
                return 1;
            }
            catch { return 0; }
        }

        public Category CheckDuplicatedCategory(string name)
        {
            var data = db.Categories.Where(x => x.Name.ToLower() == name.ToLower()).FirstOrDefault();
            return data;
        }

        public List<Category> GetByCategory(CategoryRequestModel request, int pageIndex, int recordPerPage, out int totalCount)
        {
            pageIndex = pageIndex - 1;

            var query = from s in db.Categories
                        where (string.IsNullOrEmpty(request.Keywords) || s.Name.Contains(request.Keywords))
                        select s;

            totalCount = query.Count();
            return query.OrderByDescending(x => x.Id).Skip(pageIndex * recordPerPage).Take(recordPerPage).ToList() ?? new List<Category>();
        }

        public List<Category> GetAll()
        {
            var query = from s in db.Categories
                        select s;

            return query.ToList() ?? new List<Category>();
        }
        public Category GetById(int? id)
        {
            return db.Categories.FirstOrDefault(x => x.Id == id) ?? new Category();
        }
    }
}