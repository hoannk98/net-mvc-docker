﻿using NetMVCBuilder.IRepository;
using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.RequestModel;
using NetMVCBuilder.Models.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace NetMVCBuilder.Repository
{
    public class ProductRepository : IProductRepository
    {
        private MaterialsContext db = new MaterialsContext();

        public int Add(ProductInfo sanPham)
        {
            using (var dbContext = new MaterialsContext())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        Product sp = new Product()
                        {
                            CreatedDate = DateTime.Now,
                            ModifiedDate = DateTime.Now,
                            Code = sanPham.Code,
                            Name = sanPham.Name,
                            Quantity = sanPham.Quantity,
                            Brand = sanPham.Brand,
                            CreatedBy = sanPham.ModifiedBy,
                            ModifiedBy = sanPham.ModifiedBy,
                            CategoryId = sanPham.CategoryId,
                            Price = sanPham.Price,
                            Description = sanPham.Description,
                            PictureThumbnail = sanPham.PictureThumbnail,
                            Status = 1
                        };
                        dbContext.Product.Add(sp);
                        dbContext.SaveChanges();

                        if (sanPham.lstProductDetail.Count > 0)
                        {
                            foreach (var item in sanPham.lstProductDetail)
                            {
                                ProductDetail ct = new ProductDetail()
                                {
                                    ColorId = item.ColorId,
                                    CreatedDate = DateTime.Now,
                                    ModifiedDate = DateTime.Now,
                                    Price = item.Price,
                                    Quantity = item.Quantity,
                                    ProductId = sp.Id,
                                    SizeId = item.SizeId,
                                };
                                dbContext.ProductDetails.Add(ct);
                                dbContext.SaveChanges();
                            }
                            foreach (var path in sanPham.PicturePaths)
                            {
                                ProductImage pi = new ProductImage()
                                {
                                    ProductId = sp.Id,
                                    CreatedDate = DateTime.Now,
                                    ModifiedDate = DateTime.Now,
                                    CreatedBy = sp.CreatedBy,
                                    ModifiedBy = sp.ModifiedBy,
                                    ImagePath = path,
                                };
                                dbContext.ProductImages.Add(pi);
                                dbContext.SaveChanges();
                            }

                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                            return 0;
                        }
                        return 1;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return 0;
                    }
                }
            }

        }

        public int AddProductDetail(ProductDetail ct)
        {
            try
            {
                ct.CreatedDate = DateTime.Now;
                ct.ModifiedDate = DateTime.Now;
                db.ProductDetails.Add(ct);
                db.SaveChanges();
                return 1;
            }
            catch { return 0; }
        }

        public ProductDetail CheckDuplicatedChiTietSanPham(int? size, int? color, int san_pham_id)
        {
            var san_pham = db.Product.FirstOrDefault(x => x.Id == san_pham_id) ?? new Product();
            var data = db.ProductDetails.Where(x => x.ProductId == san_pham.Id && x.SizeId == size && x.ColorId == color).FirstOrDefault();
            return data;
        }

        public Product GetByMaSanPham(string ma_san_pham, int? id)
        {
            var data = db.Product.Where(x => x.Code.ToLower() == ma_san_pham.ToLower() && x.Id != id).FirstOrDefault();
            return data;
        }

        public ProductInfo GetById(int id)
        {
            var data = (from p in db.Product
                        join c in db.Categories on p.CategoryId equals c.Id
                        where p.Id == id
                        select new ProductInfo()
                        {
                            Id = p.Id,
                            CategoryId = p.CategoryId,
                            Code = p.Code,
                            Description = p.Description,
                            Name = p.Name,
                            Quantity = p.Quantity,
                            Brand = p.Brand,
                            PictureThumbnail = p.PictureThumbnail,
                            Status = p.Status,
                            Price = p.Price,
                            QuantityCurrent = p.QuantityCurrent,
                            CategoryName = c.Name,
                        }).FirstOrDefault() ?? new ProductInfo();
            data.lstProductDetail = (from pd in db.ProductDetails
                                     join p in db.Product on pd.ProductId equals p.Id
                                     join c in db.Colors on pd.ColorId equals c.Id
                                     join s in db.Sizes on pd.SizeId equals s.Id
                                     where pd.ProductId == data.Id
                                     select new ProductDetailInfo()
                                     {
                                         Id = pd.Id,
                                         ProductId = pd.ProductId,
                                         ColorId = pd.ColorId,
                                         ColorName = c.Name,
                                         SizeId = pd.SizeId,
                                         SizeName = s.Name,
                                         Price = pd.Price,
                                         CreatedDate = pd.CreatedDate,
                                         ModifiedDate = pd.ModifiedDate,
                                         ProductName = p.Name,
                                         Quantity = pd.Quantity,
                                         QuantityCurrent = pd.QuantityCurrent,
                                         QuantityLimit = pd.QuantityLimit,
                                     }).ToList() ?? new List<ProductDetailInfo>();

            data.ListImage = db.ProductImages.Where(x => x.ProductId == data.Id).ToList() ?? new List<ProductImage>();
            var lstSameProduct = db.Product.Where(x => x.CategoryId == data.CategoryId).ToList() ?? new List<Product>();
            var currentProduct = db.Product.Where(x => x.Id == id).ToList() ?? new List<Product>();
            data.ListSameProduct = lstSameProduct.Except(currentProduct).OrderByDescending(x => x.CreatedDate).Take(4).ToList();
            var lstDetail = db.ProductDetails.Where(x => x.ProductId == data.Id).ToList() ?? new List<ProductDetail>();
            var lstSizeId = lstDetail.Select(x => x.SizeId).Distinct().ToList() ?? new List<int?>();
            var lstSize = db.Sizes.Where(x => lstSizeId.Contains(x.Id)).Select(x => x.Name).ToList() ?? new List<string>();
            data.lstSize = String.Join(", ", lstSize);
            var lstColorId = lstDetail.Select(x => x.ColorId).Distinct().ToList() ?? new List<int?>();
            var lstColor = db.Colors.Where(x => lstSizeId.Contains(x.Id)).Select(x => x.Name).ToList() ?? new List<string>();
            data.lstColor = String.Join(", ", lstColor);
            return data;
        }

        public List<ProductInfo> GetByProduct(ProductRequestModel request, int pageIndex, int recordPerPage, out int totalCount)
        {
            pageIndex = pageIndex - 1;
            if (!string.IsNullOrEmpty(request.Keywords))
            {
                request.Keywords = request.Keywords.Trim();
            }

            var query = from s in db.Product
                        where (string.IsNullOrEmpty(request.Keywords) || s.Code.Contains(request.Keywords) || s.Name.Contains(request.Keywords))
                        select new ProductInfo()
                        {
                            Id = s.Id,
                            Code = s.Code,
                            Name = s.Name,
                            Quantity = s.Quantity,
                            Brand = s.Brand,
                            CreatedDate = s.CreatedDate,
                            ModifiedDate = s.ModifiedDate,
                            CreatedBy = s.CreatedBy,
                            ModifiedBy = s.ModifiedBy,
                            QuantityCurrent = s.QuantityCurrent,
                            CategoryId = s.CategoryId,
                            Price = s.Price,
                        };

            totalCount = query.Count();
            if (totalCount > 0)
            {
                var rs = query.OrderByDescending(x => x.Id).Skip(pageIndex * recordPerPage).Take(recordPerPage).ToList();
                foreach (var item in rs)
                {
                    item.lstProductDetail = (from c in db.ProductDetails
                                             where c.ProductId == item.Id
                                             join s in db.Sizes on c.SizeId equals s.Id
                                             join co in db.Colors on c.ColorId equals co.Id
                                             select new ProductDetailInfo()
                                             {
                                                 Id = c.Id,
                                                 ColorId = c.ColorId,
                                                 SizeId = c.SizeId,
                                                 ProductId = c.ProductId,
                                                 CreatedDate = c.CreatedDate,
                                                 ModifiedDate = c.ModifiedDate,
                                                 Price = c.Price,
                                                 Quantity = c.Quantity,
                                                 ColorName = co.Name,
                                                 SizeName = s.Name,
                                                 QuantityCurrent = c.QuantityCurrent,
                                                 QuantityLimit = c.QuantityLimit
                                             }).ToList() ?? new List<ProductDetailInfo>();
                }
                return rs;
            }

            return new List<ProductInfo>();
        }

        public Product GetByTenSanPham(string ten_san_pham, int? id)
        {
            var data = db.Product.Where(x => x.Name.ToLower() == ten_san_pham.ToLower() && x.Id != id).FirstOrDefault();
            return data;
        }

        public List<ProductDetailInfo> GetListDetailByProductId(ProductRequestModel request, int pageIndex, int recordPerPage, out int totalCount)
        {
            pageIndex = pageIndex - 1;
            if (!string.IsNullOrEmpty(request.Keywords))
            {
                request.Keywords = request.Keywords.Trim();
            }

            var query = from c in db.ProductDetails
                        where c.ProductId == request.ProductId
                        join s in db.Sizes on c.SizeId equals s.Id
                        join cl in db.Colors on c.ColorId equals cl.Id
                        where (string.IsNullOrEmpty(request.Keywords) || s.Name.Contains(request.Keywords) || cl.Name.Contains(request.Keywords))
                        select new ProductDetailInfo()
                        {
                            Id = c.Id,
                            ColorId = c.ColorId,
                            SizeId = c.SizeId,
                            ColorName = cl.Name,
                            SizeName = s.Name,
                        };

            totalCount = query.Count();
            return query.OrderByDescending(x => x.Id).Skip(pageIndex * recordPerPage).Take(recordPerPage).ToList() ?? new List<ProductDetailInfo>();
        }

        public ProductRequestModel GetBySanPham(string KeySearch, int? categoryId, int page, int pageSize)
        {
            ProductRequestModel request = new ProductRequestModel();
            List<Product> lst = new List<Product>();
            request.page = page;
            request.pageSize = pageSize;
            lst = db.Product.ToList();
            int startRow = (page - 1) * pageSize;
            if (!string.IsNullOrEmpty(KeySearch))
            {
                KeySearch = KeySearch.ToLowerInvariant();
                lst = lst.Where(x => x.Name.ToLowerInvariant().Contains(KeySearch) ||
                                     x.Code.ToLowerInvariant().Contains(KeySearch)).ToList();
            }
            if (categoryId != null)
            {
                lst = lst.Where(x => x.CategoryId == categoryId).ToList();
            }
            request.totalRecord = lst.Count;
            request.data = lst.OrderByDescending(x => x.Id)
                .Skip(startRow)
                .Take(pageSize)
                .ToList();
            int totalPage = 0;
            if (request.totalRecord % pageSize == 0)
            {
                totalPage = request.totalRecord / pageSize;
            }
            else
            {
                totalPage = request.totalRecord / pageSize + 1;
            }
            request.totalPage = totalPage;
            return request;
        }

        public int DeleteProductDetail(int id)
        {
            try
            {
                var data = db.ProductDetails.Where(x => x.Id == id).FirstOrDefault();
                db.ProductDetails.Remove(data);
                db.SaveChanges();
                return 1;
            }
            catch { return 0; }
        }
        public int Edit(ProductInfo productInfo)
        {
            using (var dbContext = new MaterialsContext())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var productFind = dbContext.Product.Where(x => x.Id == productInfo.Id).FirstOrDefault();
                        if (productFind==null)
                        {
                            return 0;
                        }
                        productFind.ModifiedDate = DateTime.Now;
                        productFind.Code = productInfo.Code;
                        productFind.Name = productInfo.Name;
                        productFind.Quantity = productInfo.Quantity;
                        productFind.Brand = productInfo.Brand;
                        productFind.ModifiedBy = productInfo.ModifiedBy;
                        productFind.CategoryId = productInfo.CategoryId;
                        productFind.Price = productInfo.Price;
                        productFind.Description = productInfo.Description;
                        if (productInfo.PictureThumnailFile != null)
                        {
                            productFind.PictureThumbnail = productInfo.PictureThumbnail;
                        }
                        
                        dbContext.SaveChanges();

                        if (productInfo.lstProductDetail.Count > 0)
                        {
                            var productDetail = dbContext.ProductDetails.Where(x => x.ProductId == productFind.Id);
                            dbContext.ProductDetails.RemoveRange(productDetail);
                            foreach (var item in productInfo.lstProductDetail)
                            {
                                ProductDetail ct = new ProductDetail()
                                {
                                    ColorId = item.ColorId,
                                    CreatedDate = DateTime.Now,
                                    ModifiedDate = DateTime.Now,
                                    Price = item.Price,
                                    Quantity = item.Quantity,
                                    ProductId = productFind.Id,
                                    SizeId = item.SizeId,
                                };
                                dbContext.ProductDetails.Add(ct);
                                dbContext.SaveChanges();
                            }
                            if (productInfo.PictureFiles != null)
                            {
                                var productImage = dbContext.ProductImages.Where(x => x.ProductId == productFind.Id);
                                dbContext.ProductImages.RemoveRange(productImage);
                                foreach (var path in productInfo.PicturePaths)
                                {
                                    ProductImage pi = new ProductImage()
                                    {
                                        ProductId = productFind.Id,
                                        CreatedDate = DateTime.Now,
                                        ModifiedDate = DateTime.Now,
                                        CreatedBy = productFind.CreatedBy,
                                        ModifiedBy = productFind.ModifiedBy,
                                        ImagePath = path,
                                    };
                                    dbContext.ProductImages.Add(pi);
                                    dbContext.SaveChanges();
                                }
                            }
                            transaction.Commit();
                        }
                        else
                        {
                            transaction.Rollback();
                            return 0;
                        }
                        return 1;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return 0;
                    }
                }
            }

        }

        public int Delete(int id)
        {
            using (var dbContext = new MaterialsContext())
            {
                using (DbContextTransaction transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        var productData = db.Product.FirstOrDefault(x => x.Id == id);
                        dbContext.Product.Attach(productData);
                        dbContext.Product.Remove(productData);
                        var productDetail = dbContext.ProductDetails.Where(x => x.ProductId == id);
                        dbContext.ProductDetails.RemoveRange(productDetail);
                        var productImage = dbContext.ProductImages.Where(x => x.ProductId == id);
                        dbContext.ProductImages.RemoveRange(productImage);
                        dbContext.SaveChanges();
                        transaction.Commit();
                        return 1;
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return 0;
                    }
                }
            }
            
        }
    }
}