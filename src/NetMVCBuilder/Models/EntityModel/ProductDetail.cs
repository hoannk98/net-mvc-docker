﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Models.EntityModel
{
    [Table("ProductDetail")]
    public class ProductDetail
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public int? SizeId { get; set; }
        public int? Quantity { get; set; }// số lượng nhập từ trước đến giờ
        public int? QuantityCurrent { get; set; }// số lượng hiện tại trong kho
        public int? QuantityLimit { get; set; }// Giới hạn số lượng báo động hết hàng
        public int? ColorId { get; set; }
        public decimal? Price { get; set; } // giá nhập của sản phẩm
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}