namespace NetMVCBuilder.Models.EntityModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Product")]
    public partial class Product
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Code { get; set; }

        public int? Quantity { get; set; }

        public int? QuantityCurrent { get; set; }

        public string Brand { get; set; }

        public string PictureThumbnail { get; set; }

        public int? CategoryId { get; set; }

        public string Description { get; set; }

        public decimal? Price { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? CreatedBy { get; set; }

        public int? ModifiedBy { get; set; }

        public int? Status { get; set; }

    }
}
