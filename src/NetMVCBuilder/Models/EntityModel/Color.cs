﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Models.EntityModel
{
    [Table("Color")]
    public class Color
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}