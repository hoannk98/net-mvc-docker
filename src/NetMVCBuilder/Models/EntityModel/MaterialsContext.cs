namespace NetMVCBuilder.Models.EntityModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using MySql.Data.Entity;

    //[DbConfigurationType(typeof(MySqlEFConfiguration))]
    public partial class MaterialsContext : DbContext
    {
        public MaterialsContext()
            : base("name=MaterialsContext")
        {
        }

        public virtual DbSet<Action> Actions { get; set; }
        public virtual DbSet<Module> Moduls { get; set; }
        public virtual DbSet<Page> Pages { get; set; }
        public virtual DbSet<PermissionAction> PermissionActions { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserGroup> UserGroups { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<NewsArticle> NewsArticles { get; set; }
        public DbSet<Size> Sizes { get; set; }
        public DbSet<Color> Colors { get; set; }
        public virtual DbSet<ProductDetail> ProductDetails { get; set; }
        public virtual DbSet<ProductImage> ProductImages { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<MessageContact> MessageContacts { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }

        //public MaterialsContext(string connectionString)
        //    : base(connectionString) { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Action>()
                .Property(e => e.Description)
                .IsFixedLength();

            modelBuilder.Entity<Module>()
                .Property(e => e.Description)
                .IsFixedLength();

            modelBuilder.Entity<Page>()
                .Property(e => e.Description)
                .IsFixedLength();

            modelBuilder.Entity<Role>()
                .Property(e => e.Description)
                .IsFixedLength();

            modelBuilder.Entity<User>()
                .Property(e => e.FullName)
                .IsFixedLength();

            modelBuilder.Entity<UserRole>()
                .Property(e => e.Description)
                .IsFixedLength();

            modelBuilder.Entity<UserGroup>()
                .Property(e => e.UserGroupName)
                .IsFixedLength();

            modelBuilder.Entity<Product>()
                .Property(e => e.Name)
                .IsFixedLength();
            
            modelBuilder.Entity<NewsArticle>()
               .Property(e => e.Name)
               .IsFixedLength();
            modelBuilder.Entity<Size>()
               .Property(e => e.Name)
               .IsFixedLength();
            modelBuilder.Entity<Color>()
              .Property(e => e.Name)
              .IsFixedLength();
            modelBuilder.Entity<ProductDetail>()
              .Property(e => e.ProductId);
            modelBuilder.Entity<ProductImage>()
              .Property(e => e.ImagePath)
              .IsFixedLength();
            modelBuilder.Entity<Category>()
              .Property(e => e.Name)
              .IsFixedLength();
            modelBuilder.Entity<MessageContact>()
              .Property(e => e.Name)
              .IsFixedLength();
            modelBuilder.Entity<Contact>()
              .Property(e => e.CompanyName)
              .IsFixedLength();
        }
    }
}
