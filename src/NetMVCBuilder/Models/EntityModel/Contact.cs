﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace NetMVCBuilder.Models.EntityModel
{
    [Table("Contact")]
    public class Contact
    {
        public int Id { get; set; }

        public string CompanyName { get; set; }
        public string BusinessRegistrationCertificate { get; set; }
        public string CertificateDate { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string TiktokAddress { get; set; }
        public string YouTubeAddress { get; set; }
        public string FacebookAddress { get; set; }
        public string ZaloAddress { get; set; }
        public string MessengerAddress { get; set; }
        public string CompanySummary { get; set; }
        public string PhoneNumber { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? CreatedBy { get; set; }

        public int? ModifiedBy { get; set; }
    }
}