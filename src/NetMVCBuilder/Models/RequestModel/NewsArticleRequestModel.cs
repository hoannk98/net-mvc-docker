﻿using NetMVCBuilder.Models.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Models.RequestModel
{
    public class NewsArticleRequestModel : BaseModel
    {
        public string Keywords { get; set; }
        public override object RouteValues => new
        {
            this.Keywords,
            this.FromDate,
            this.ToDate,
        };
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public List<NewsArticle> data { get; set; }
        public bool? isHome { get; set; }
    }
}