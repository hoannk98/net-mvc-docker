﻿using NetMVCBuilder.Models.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Models.RequestModel
{
    public class MediaSelectorSearchQuery : BaseModel
    {
        public int Page { get; set; }
        public int? Id { get; set; }
        public int? CategoryId { get; set; }

        public string Query { get; set; }
        public List<ProductImage> data { get; set; }
    }
}