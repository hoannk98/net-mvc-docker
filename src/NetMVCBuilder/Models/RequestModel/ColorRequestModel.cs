﻿using System;
using NetMVCBuilder.Models.EntityModel;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Models.RequestModel
{
    public class ColorRequestModel : BaseModel
    {
        public string Keywords { get; set; }
        public override object RouteValues => new
        {
            this.Keywords
        };
        public List<Color> data { get; set; }
    }
}