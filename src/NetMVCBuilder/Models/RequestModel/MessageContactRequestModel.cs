﻿using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetMVCBuilder.Models.RequestModel
{
    public class MessageContactRequestModel : BaseModel
    {
        [AllowHtml]
        public string Keywords { get; set; }
        public override object RouteValues => new
        {
            this.Keywords
        };

        public List<MessageContact> Data { get; set; }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public DateTime ModifyDate { get; set; }
        public DateTime CreateDate { get; set; }
    }
}