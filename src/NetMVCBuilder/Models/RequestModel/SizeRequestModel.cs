﻿using NetMVCBuilder.Models.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Models.RequestModel
{
    public class SizeRequestModel : BaseModel
    {
        public string Keywords { get; set; }
        public override object RouteValues => new
        {
            this.Keywords
        };
        public List<Size> data { get; set; }
    }
}