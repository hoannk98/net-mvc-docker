﻿using NetMVCBuilder.Models.EntityModel;
using NetMVCBuilder.Models.InfoModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetMVCBuilder.Models.RequestModel
{
    public class ProductRequestModel: BaseModel
    {
        [AllowHtml]
        public string Keywords { get; set; }
        public override object RouteValues => new
        {
            this.Keywords
        };
        public List<ProductInfo> DataInfo { get; set; }

        public int? Size { get; set; }
        public int? Color { get; set; }
        public int? ProductId { get; set; }

        public List<Product> data { get; set; }

        public List<ProductInfo> ListNewProduct { get; set; }
        public List<NewsArticle> ListNewsArticle { get; set; }
    }
}