﻿using NetMVCBuilder.Models.EntityModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NetMVCBuilder.Models.InfoModel
{
    public class ProductInfo
    {
        public int Id { get; set; }

        [StringLength(20)]
        [Display(Name = "Mã sản phẩm")]
        [Required(ErrorMessage = "Vui lòng nhập mã sản phẩm")]
        [Remote("CheckDuplicatedMaSanPham", "Product", AdditionalFields = "id", HttpMethod = "POST", ErrorMessage = "Mã sản phẩm đã tồn tại")]
        public string Code { get; set; }

        [StringLength(500)]
        [Display(Name = "Tên sản phẩm")]
        [Required(ErrorMessage = "Vui lòng nhập tên sản phẩm")]
        [Remote("CheckDuplicatedTenSanPham", "Product", AdditionalFields = "id", HttpMethod = "POST", ErrorMessage = "Tên sản phẩm đã tồn tại")]
        public string Name { get; set; }

        public int? Quantity { get; set; }

        public int? QuantityCurrent { get; set; }

        public string Brand { get; set; }

        public string PictureThumbnail { get; set; }

        public int? CategoryId { get; set; }

        public string Description { get; set; }

        public decimal? Price { get; set; }

        public DateTime? CreatedDate { get; set; }

        public DateTime? ModifiedDate { get; set; }

        public int? CreatedBy { get; set; }

        public int? ModifiedBy { get; set; }

        public int? Status { get; set; }

        public List<ProductDetailInfo> lstProductDetail { get; set; }

        public HttpPostedFileBase PictureThumnailFile { get; set; }
        public List<HttpPostedFileBase> PictureFiles { get; set; }

        public List<string> PicturePaths { get; set; }
        public string lstStrProductDetails { get; set; }

        public List<ProductImage> ListImage { get; set; }
        public List<Product> ListSameProduct { get; set; }
        public string lstSize { get; set; }
        public string lstColor { get; set; }
        public string CategoryName { get; set; }
    }
}