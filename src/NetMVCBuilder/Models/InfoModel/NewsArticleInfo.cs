﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Models.InfoModel
{
    public class NewsArticleInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Summary { get; set; }
        public string PictureThumbnail { get; set; }
        public string ContentNews { get; set; }
        public string Author { get; set; }
        public int? Status { get; set; }
        public int? IsHot { get; set; }
        public int? NewsCategoryId { get; set; }
        public bool? IsDeleted { get; set; }
        public int? Type { get; set; }
        public string UrlSegment { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? CreatedBy { get; set; }
        public int? ModifiedBy { get; set; }
        public HttpPostedFileBase PictureFiles { get; set; }
    }
}