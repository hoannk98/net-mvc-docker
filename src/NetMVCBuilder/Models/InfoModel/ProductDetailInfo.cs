﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Models.InfoModel
{
    public class ProductDetailInfo
    {
        public int Id { get; set; }
        public int? ProductId { get; set; }
        public string ProductName { get; set; }
        public int? SizeId { get; set; }
        public string SizeName { get; set; }
        public int? Quantity { get; set; }// số lượng nhập từ trước đến giờ
        public int? QuantityCurrent { get; set; }// số lượng hiện tại trong kho
        public int? QuantityLimit { get; set; }// Giới hạn số lượng báo động hết hàng
        public int? ColorId { get; set; }
        public string ColorName { get; set; }
        public decimal? Price { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string UnitName { get; set; }
    }
}