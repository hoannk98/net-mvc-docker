﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NetMVCBuilder.Models
{
    public class LoginModel
    {
        public LoginModel()
        {
            FailedLogins = 0;
        }

        [Key]
        [Display(Name = "Tên đăng nhập")]
        [Required(ErrorMessage = "Tài khoản không được để trống")]
        public string UserName { set; get; }

        [Required(ErrorMessage = "Mật khẩu không được để trống")]
        [Display(Name = "Mật khẩu")]
        public string Password { set; get; }

        public bool RememberMe { set; get; }
        public int FailedLogins { get; set; }

        public string Captcha { get; set; }
    }
}