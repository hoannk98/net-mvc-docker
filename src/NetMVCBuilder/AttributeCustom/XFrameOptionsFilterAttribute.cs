﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace NetMVCBuilder.AttributeCustom
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class XFrameOptionsFilterAttribute : ActionFilterAttribute
    {
        private string _xframeOptions = "X-Frame-Options";
        private string _contentSecurityPolicy = "Content-Security-Policy";

        public bool AlwaysApply { get; set; }

        public XFrameOptionsFilterAttribute() { }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            var response = filterContext.HttpContext.Response;

            // Assume that if we aren't suppressing the header, and it's not present, then we don't want the view suppressed.
            // This ties in better with the AntiForgery token expected behavior.  Otherwise, if the header is suppressed,
            // then we apply the filter to every response.
            if (!AntiForgeryConfig.SuppressXFrameOptionsHeader && !response.Headers.AllKeys.Any(x => x == _xframeOptions) && !AlwaysApply)
            {
                return;
            }

            // If the AntiForgeryConfig is set to add the SAMEORIGIN header, and we've made it this far, remove it.
            if (!AntiForgeryConfig.SuppressXFrameOptionsHeader)
            {
                response.Headers.Remove(_xframeOptions);
            }

            // If the refer is in the list, do nothing.  If they aren't, add SAMEORIGIN
            // This is assuming that browers all check the SAMEORIGIN policy on first loading and not subsequent refreshes.
            if (filterContext.HttpContext.Request.UrlReferrer != null &&
                (filterContext.HttpContext.Request.Url.Host != filterContext.HttpContext.Request.UrlReferrer.Host))
            {
                var requestUrl = filterContext.HttpContext.Request.UrlReferrer.GetLeftPart(UriPartial.Authority);
                //if (!_whiteList.Any(x => x.Url.StartsWith(requestUrl)))
                //{
                //    response.AddHeader(_xframeOptions, "SAMEORIGIN");
                //}
            }
        }

        //private List<WhiteList> GetWhiteList(object[] param)
        //{
        //    // Get your whitelist from where ever
        //    return whiteList;
        //}
    }
}