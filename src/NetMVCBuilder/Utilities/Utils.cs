﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Web;
using Encoder = System.Drawing.Imaging.Encoder;

namespace Namek.Core.Utility
{
    public static class Utils
    {
        private static string[] whiteList = { "jpg", "jpeg", "png", "svg", "bmp", "tif", "tiff", "gif" };
        public static string EmbedVideo(string url, string width, string height)
        {
            var result = "";
            result += "<embed width=\"" + width + "\" height=\"" + height + "\"";
            result += "flashvars=\"file=" + url + "&amp;volume=60&amp;repeat=false&amp;bufferlength=10&amp;";
            //result += "image=" + image + "\"";
            result += " allowscriptaccess=\"always\" allowfullscreen=\"true\" wmode=\"transparent\" quality=\"hight\"";
            result +=
                "src=\"/images/flvplayer.swf\" type=\"application/x-shockwave-flash\" name=\"flvplayer\" id=\"flvplayer\"></embed> ";
            return result;
        }

        public static bool IsNumber(string value)
        {
            int number1;
            return int.TryParse(value, out number1);
        }

        public static double ConvertToDateTimeStamp(DateTime inputDateTime)
        {
            var span = inputDateTime - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();
            return span.TotalSeconds;
        }

        private static void Empty(this DirectoryInfo directory)
        {
            foreach (var file in directory.GetFiles()) file.Delete();
            foreach (var subDirectory in directory.GetDirectories()) subDirectory.Delete(true);
        }

        public static void DeleteFolder(string uploadPath)
        {
            try
            {
                if (!Directory.Exists(uploadPath))
                    return;

                var directory = new DirectoryInfo(uploadPath);
                foreach (var file in directory.GetFiles())
                    file.Delete();
                directory.Empty();
                directory.Delete();
            }
            catch (Exception ex)
            {
            }
        }

        public static void DeleteFile(string uploadPath, string fileName)
        {
            try
            {
                if (!Directory.Exists(uploadPath))
                    return;
                             
                var filePath = uploadPath + fileName;
               
                File.Delete(filePath);
            }
            catch (Exception ex)
            {
                
            }
        }

        public static string DeleteFile(HttpRequest request, string uploadPath, string fileName, string entityName)
        {
            try
            {
                if (!Directory.Exists(uploadPath))
                    return "fail";
                              
                var filePath = uploadPath + fileName;
                
                File.Delete(filePath);

                var directory = new DirectoryInfo(uploadPath);
                foreach (var file in directory.GetFiles())
                    if (file.Name.Contains(fileName + "."))
                        file.Delete();

                return "success";
            }
            catch (Exception ex)
            {
                return "fail";
            }
        }

        public static string Upload(HttpRequest request, string uploadPath, string entityName, string fileExt,
            string filePrefix, string fileName)
        {
            if (request == null)
                return "fail|request_error";

            var httpPostedFile = request.Files[0];
            
            var allowContentType = ConfigurationManager.AppSettings["ImageContenType"];
            if (!string.IsNullOrEmpty(allowContentType))
            {
                var contentType = httpPostedFile.ContentType;
                if (allowContentType.IndexOf(contentType, StringComparison.Ordinal) == -1)
                    return "fail|denied_content_type";
            }

            if (!Directory.Exists(uploadPath))
                Directory.CreateDirectory(uploadPath);
            
            var strBuilder = new StringBuilder();
            strBuilder.Append(fileName).Append(".").Append(fileExt);

            var filePath = uploadPath + strBuilder;
            
            httpPostedFile.SaveAs(filePath);

            var strReturn = "success|" + fileName + "." + fileExt;

            return strReturn;
        }

        private static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            var encoders = ImageCodecInfo.GetImageEncoders();
            return encoders.FirstOrDefault(t => t.MimeType == mimeType);
        }

        public static class FileTypeUploadSettings
        {
            private const string DefaultAllowedFileTypes = ".gif,.jpeg,.jpg,.png,.svg,.bmp,.tif,.tiff";

            public static IEnumerable<string> AllowedFileTypeList
            {
                get { return Array.ConvertAll(DefaultAllowedFileTypes.Split(','), p => p.Trim()); }
            }
        }

        public static byte[] convertImagetoByte(Image x)
        {
            ImageConverter _imageConverter = new ImageConverter();
            byte[] xByte = (byte[])_imageConverter.ConvertTo(x, typeof(byte[]));
            return xByte;
        }

        public static byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }

        public static bool CheckFileExtension(string filename, out string extension)
        {
            var arrPath = filename.Split('.');
            extension = arrPath[arrPath.Length - 1].ToLower();
            return whiteList.Contains(extension);
        }

        public static System.Drawing.Image resizeImageThumbnail(System.Drawing.Image imgToResize)
        {
            int destWidth = 1200; //New Width  
            int destHeight = 630; //New Height  
            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            // Draw image with new width and height  
            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();
            return (System.Drawing.Image)b;
        }

        public static string DecodeUrlString(string url)
        {
            string newUrl;
            while ((newUrl = Uri.UnescapeDataString(url)) != url)
                url = newUrl;
            return newUrl;
        }
    }

}